#!/usr/bin/bash
# Copyright © 2019-2020 Thierry Simeao
#
# This file is part of Lampadaire cabalistique.
#
# Lampadaire cabalistique is free software: you can redistribute it and/or modify it under the terms of the GNU Affero
# General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
# option) any later version.
#
# Lampadaire cabalistique is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
# implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License
# for more details.
#
# You should have received a copy of the GNU Affero General Public License along with Lampadaire cabalistique.
# If not, see <https://www.gnu.org/licenses/>.

# check execution conditions
if [[ -z ${GITLAB_CI} ]]; then
	echo "$0: This script is designed to be executed in GitLab CI environment" >&2
	exit 1
fi

if [[ $(uname -s) != 'Linux' ]]; then
	echo "$0: This script is designed to be executed on Arch Linux" >&2
	exit 1
fi

if [[ $(whoami) == 'root' ]]; then
	echo "$0: Superuser mustn’t execute this script" >&2
	exit 1
fi

# set script environment
if [[ ${CI_JOB_NAME} =~ ^(android|pages)(:.+)?$ ]]; then
	CMAKE_GENERATOR=Ninja
else
	CMAKE_GENERATOR='Unix Makefiles'
fi

# update packages
sudo pacman --sync --refresh --sysupgrade --quiet --noconfirm --noprogressbar --color=always

# finalize configuration
git config --global user.name "Continuous Integration (${CI_COMMIT_SHORT_SHA})"
git config --global user.email gitl@b.com

echo -e '\e[1;32m⇒\e[37m Generate build environment…\e[0m'
cmake -Wno-dev -DCMAKE_BUILD_TYPE:STRING=Release -DCMAKE_SKIP_INSTALL_RULES:BOOL=ON \
	"-DPAGES_DIR:PATH=${CI_PROJECT_DIR}/public" \
	"-G${CMAKE_GENERATOR}" "-S${CI_PROJECT_DIR}" "-B${CI_PROJECT_DIR}/_build"
