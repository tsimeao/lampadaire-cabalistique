# Copyright ©2020 Thierry Simeao
#
# This file is part of Lampadaire cabalistique.
#
# Lampadaire cabalistique is free software: you can redistribute it and/or modify it under the terms of the GNU Affero
# General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
# option) any later version.
#
# Lampadaire cabalistique is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
# implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License
# for more details.
#
# You should have received a copy of the GNU Affero General Public License along with Lampadaire cabalistique.
# If not, see <https://www.gnu.org/licenses/>.
cmake_minimum_required(VERSION 3.17)

find_program(DOCKER_COMMAND docker REQUIRED)

file(READ "${base-container}" container-ID)
if(NOT container-ID MATCHES "^[0-9a-f]+$" OR NOT DEFINED image-ID)
	message(FATAL_ERROR "Missing arguments to create the image ‘${image-ID}’.")
endif()

set(packages
	android/cmdline-tools
	android/ndk
	android/platform
	android/sdk-build-tools
	android/sdk-platform-tools
)

# download, build, & install the packages
execute_process(COMMAND ${DOCKER_COMMAND} container exec --user=root --workdir=/tmp/packages -- ${container-ID}
		./install.sh ${packages}
	RESULT_VARIABLE error-code)

# stop the intermediate container
execute_process(COMMAND ${DOCKER_COMMAND} container exec --user=root -- ${container-ID}
		bash -c -r -- "\
			umount -- /tmp{/makepkg,}/packages && \
			rm --recursive --force -- /home/uprooted/.parallel /tmp/{makepkg,packages}")
execute_process(COMMAND ${DOCKER_COMMAND} container stop -- ${container-ID}
	OUTPUT_QUIET)

if(NOT error-code)
	message("Create the image ‘${image-ID}’…")
	execute_process(COMMAND ${DOCKER_COMMAND} container commit
			"--change=CMD [\"/usr/bin/bash\", \"--login\"]"
			--
			${container-ID}
			"${image-ID}"
		OUTPUT_QUIET
		RESULT_VARIABLE error-code)
endif()

# remove the intermediate container
execute_process(COMMAND ${DOCKER_COMMAND} container rm -- ${container-ID}
	OUTPUT_QUIET)
file(REMOVE ${base-container})

if(error-code)
	message(FATAL_ERROR "Cannot create the image ‘${image-ID}’.")
endif()
