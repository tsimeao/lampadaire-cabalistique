#!/usr/bin/bash
# Copyright © 2019-2020 Thierry Simeao
#
# This file is part of Lampadaire cabalistique.
#
# Lampadaire cabalistique is free software: you can redistribute it and/or modify it under the terms of the GNU Affero
# General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
# option) any later version.
#
# Lampadaire cabalistique is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
# implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License
# for more details.
#
# You should have received a copy of the GNU Affero General Public License along with Lampadaire cabalistique.
# If not, see <https://www.gnu.org/licenses/>.

# check execution conditions
if [[ $(uname -s) != 'Linux' ]]; then
	echo "$0: This script is designed to be executed on Arch Linux" >&2
	exit 1
fi

if [[ $(whoami) != 'root' ]]; then
	echo "$0: This script must be executed as superuser" >&2
	exit 1
fi

echo 'Install environment for makepkg…'
pacman --sync --asdeps --quiet --noconfirm --needed --noprogressbar --color=always -- \
	awk binutils bzip2 coreutils fakeroot file findutils gnupg grep gzip libarchive ncurses parallel sed unzip xz
chown -- uprooted:users /tmp/makepkg

echo 'Download the packages…'
for directory in "$@"; do
	runuser uprooted "--command=cd -- '${directory}' && makepkg --verifysource"
done

echo 'Build the packages…'
runuser -u uprooted -- parallel --group --halt=soon,fail=1 -- \
	"cd -- '{}' && makepkg --nodeps --clean --skipchecksums ANDROID_HOME=/opt/android-sdk 2>&1" ::: "$@"

echo 'Install the packages…'
find -L /tmp/makepkg/packages -name '*.pkg.tar.zst' -exec \
	pacman --upgrade --noconfirm --noprogressbar --color=always -- '{}' +

echo 'Remove the not anymore necessary packages…'
readonly PACKAGES_TO_REMOVE=$(pacman --query --deps --unrequired --quiet)
if [[ -n ${PACKAGES_TO_REMOVE} ]]; then
	pacman --remove --recursive --nosave --noconfirm -- ${PACKAGES_TO_REMOVE}
fi
