# Copyright © 2019-2020 Thierry Simeao
#
# This file is part of Lampadaire cabalistique.
#
# Lampadaire cabalistique is free software: you can redistribute it and/or modify it under the terms of the GNU Affero
# General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
# option) any later version.
#
# Lampadaire cabalistique is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
# implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License
# for more details.
#
# You should have received a copy of the GNU Affero General Public License along with Lampadaire cabalistique.
# If not, see <https://www.gnu.org/licenses/>.

pkgname=android-sdk-platform-tools
pkgver=30.0.4
pkgrel=2
pkgdesc='Android SDK Platform-Tools'
arch=('x86_64')
url='https://developer.android.com/studio/releases/platform-tools'
license=('custom')
depends=('android-sdk' 'gcc-libs' 'zlib')
makedepends=('unzip')
source=("https://dl.google.com/android/repository/platform-tools_r${pkgver}-linux.zip")
noextract=("platform-tools_r${pkgver}-linux.zip")
sha256sums=('5be24ed897c7e061ba800bfa7b9ebb4b0f8958cc062f4b2202701e02f2725891')

prepare() {
	local files_to_ignore=(
		platform-tools/adb
		platform-tools/fastboot
		'platform-tools/systrace/*'
	)

	print_step 'Extract the useful files'
	unzip -q "platform-tools_r${pkgver}-linux.zip" -x "${files_to_ignore[@]}" && \
	print_ok
}

package() {
	cd -- platform-tools
	find -L . -type f \( \( -not -name '*.*' -or -name '*.so' \) -exec \
		install -D --mode=755 --no-target-directory -- '{}' "${pkgdir}${ANDROID_HOME}/platform-tools/{}" \; \
	-or -exec \
		install -D --mode=644 --no-target-directory -- '{}' "${pkgdir}${ANDROID_HOME}/platform-tools/{}" \; \
	\)
}
