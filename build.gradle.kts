/*
 * Copyright © 2018-2020 Thierry Simeao
 *
 * This file is part of Lampadaire cabalistique.
 *
 * Lampadaire cabalistique is free software: you can redistribute it and/or modify it under the terms of the GNU Affero
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * Lampadaire cabalistique is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License
 * for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with Lampadaire cabalistique.
 * If not, see <https://www.gnu.org/licenses/>.
 */
import org.eclipse.jgit.api.Git
import org.eclipse.jgit.lib.Constants
import org.eclipse.jgit.storage.file.FileRepositoryBuilder
import org.jetbrains.dokka.gradle.DokkaTask
import org.jetbrains.kotlin.gradle.tasks.KotlinCompile
import java.time.Instant
import java.time.ZoneId
import java.time.ZonedDateTime
import java.time.format.DateTimeFormatter

buildscript {
    repositories {
        jcenter()
        google()
    }
    dependencies {
        classpath(
            group = "com.android.tools.build", name = "gradle",
            version = properties["version.android.build"] as String
        )
        classpath(
            group = "org.jetbrains.kotlin", name = "kotlin-gradle-plugin",
            version = properties["version.kotlin"] as String
        )
        classpath(
            group = "org.eclipse.jgit", name = "org.eclipse.jgit",
            version = properties["version.jGit"] as String
        )
        classpath(
            group = "org.slf4j", name = "slf4j-nop",
            version = properties["version.SLF4J"] as String
        )
        classpath(
            group = "org.jetbrains.dokka", name = "dokka-gradle-plugin",
            version = properties["version.dokka"] as String
        )
    }
}

group = "io.gitlab.tsimeao.lampadaireCabalistique"

plugins {
    id("org.jmailen.kotlinter") version properties["version.kotlinterGradle"] as String apply false
    id("com.github.sherter.google-java-format") version properties["version.googleJavaFormatPlugin"] as String
}
allprojects {
    repositories {
        jcenter()
        google()
    }

    version = properties["projectVersion"] as String
    buildDir =
        (properties["rootBuildDir"]?.let { file(it as String) } ?: rootDir).resolve(path.drop(1).replace(':', '/'))
}
subprojects {
    group = rootProject.group as String + path.replace(':', '.').replace(' ', '_')

    apply(plugin = "org.jmailen.kotlinter")

    tasks.withType<KotlinCompile> {
        kotlinOptions {
            jvmTarget = "1.8"
            allWarningsAsErrors = false
        }
    }
    tasks.withType<JavaCompile> {
        options.isDeprecation = true
        options.encoding = "UTF-8"
        options.compilerArgs = listOf("-implicit:none")
    }
    tasks.withType<Jar> {
        manifest {
            attributes(
                "Specification-Vendor" to "Thierry Simeao",
                "Specification-Version" to project.version,
                "Implementation-Version" to revision
            )
        }
    }
    tasks.withType<Test> {
        properties["libraryPath"]?.let {
            systemProperty("java.library.path", it as String)
        }
    }
    tasks.withType<DokkaTask> {
        dokkaSourceSets.all {
            includeNonPublic.set(false)
            skipDeprecated.set(true)
            reportUndocumented.set(true)
            skipEmptyPackages.set(true)
            jdkVersion.set(8)
            noStdlibLink.set(false)
            noJdkLink.set(false)
            noAndroidSdkLink.set(false)
        }
    }
}
googleJavaFormat {
    options(mapOf("style" to "AOSP"))
}

val revision: String
    get() {
        val repository = FileRepositoryBuilder().readEnvironment().findGitDir(rootDir).build()
        val git = Git(repository)

        try {
            val commit = git.log().add(repository.resolve(Constants.HEAD)).setMaxCount(1).call().first()

            return if (git.status().call().isClean)
                "${commit.name.substring(0, 7)} committed on ${
                    Instant.ofEpochSecond(commit.commitTime.toLong()).atZone(ZoneId.systemDefault())
                        .format(DateTimeFormatter.RFC_1123_DATE_TIME)
                }"
            else
                "${commit.name}-dirty compiled on ${ZonedDateTime.now().format(DateTimeFormatter.RFC_1123_DATE_TIME)}"
        } finally {
            git.close()
            repository.close()
        }
    }
