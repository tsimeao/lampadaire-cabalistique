#!/usr/bin/env -S cmake -Dshebang:BOOL=ON -P
# Copyright © 2019-2020 Thierry Simeao
#
# This file is part of Lampadaire cabalistique.
#
# Lampadaire cabalistique is free software: you can redistribute it and/or modify it under the terms of the GNU Affero
# General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
# option) any later version.
#
# Lampadaire cabalistique is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
# implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License
# for more details.
#
# You should have received a copy of the GNU Affero General Public License along with Lampadaire cabalistique.
# If not, see <https://www.gnu.org/licenses/>.
cmake_minimum_required(VERSION 3.17)

get_filename_component(script-directory ${CMAKE_SCRIPT_MODE_FILE} DIRECTORY)

if(shebang)
	get_filename_component(script-name ${CMAKE_SCRIPT_MODE_FILE} NAME)
	if(script-name STREQUAL "configure")
		execute_process(COMMAND ${CMAKE_COMMAND}
				-D CMAKE_BUILD_TYPE:STRING=Release
				-D CMAKE_SKIP_INSTALL_RULES:BOOL=OFF
				-G "Unix Makefiles"
				-S ${script-directory}
				-B ${CMAKE_SOURCE_DIR}
			RESULT_VARIABLE error-code)

		if(error-code)
			message(SEND_ERROR "Cannot generate the build environment.")
		endif()
		return()
	endif()
endif()

# Debug build tree -----------------------------------------------------------------------------------------------------
execute_process(COMMAND ${CMAKE_COMMAND}
		-D CMAKE_BUILD_TYPE:STRING=Debug
		-D CMAKE_SKIP_INSTALL_RULES:BOOL=ON
		-G "Unix Makefiles"
		-S ${script-directory}/..
		-B _build/debug
	RESULT_VARIABLE error-code)

if(error-code)
	message(SEND_ERROR "Cannot generate the ‘Debug’ configuration.")
endif()

# Release build tree ---------------------------------------------------------------------------------------------------
execute_process(COMMAND ${CMAKE_COMMAND}
		-D CMAKE_BUILD_TYPE:STRING=Release
		-G "Unix Makefiles"
		-S ${script-directory}/..
		-B _build/release
	RESULT_VARIABLE error-code)

if(error-code)
	message(SEND_ERROR "Cannot generate the ‘Release’ configuration.")
endif()
