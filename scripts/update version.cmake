#!/usr/bin/env -S cmake -P
# Copyright © 2018-2020 Thierry Simeao
#
# This file is part of Lampadaire cabalistique.
#
# Lampadaire cabalistique is free software: you can redistribute it and/or modify it under the terms of the GNU Affero
# General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
# option) any later version.
#
# Lampadaire cabalistique is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
# implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License
# for more details.
#
# You should have received a copy of the GNU Affero General Public License along with Lampadaire cabalistique.
# If not, see <https://www.gnu.org/licenses/>.
cmake_minimum_required(VERSION 3.17)

find_package(Git MODULE REQUIRED QUIET)

execute_process(COMMAND ${GIT_EXECUTABLE} rev-parse HEAD
	OUTPUT_VARIABLE GIT_REVISION OUTPUT_STRIP_TRAILING_WHITESPACE
	RESULT_VARIABLE error-code
	ERROR_QUIET)

if(NOT error-code)
	execute_process(COMMAND ${GIT_EXECUTABLE} status --porcelain
		OUTPUT_VARIABLE modified-files OUTPUT_STRIP_TRAILING_WHITESPACE
		ERROR_QUIET)

	if("${modified-files}" STREQUAL "")
		execute_process(COMMAND ${GIT_EXECUTABLE} log -1 --date=iso8601-strict --format=tformat:%cd ${GIT_REVISION}
			OUTPUT_VARIABLE GIT_REVISION_DATE OUTPUT_STRIP_TRAILING_WHITESPACE
			ERROR_QUIET)
	else()
		set(GIT_REVISION "${GIT_REVISION}-dirty")
		string(TIMESTAMP GIT_REVISION_DATE UTC)
	endif()
else()
	set(GIT_REVISION "unknown")
	string(TIMESTAMP GIT_REVISION_DATE UTC)
endif()

if(NOT DEFINED input-file AND NOT DEFINED output-file)
	message("Revision: ${GIT_REVISION} (${GIT_REVISION_DATE})")
else()
	if(EXISTS "${input-file}" AND NOT "${output-file}" STREQUAL "")
		configure_file("${input-file}" "${output-file}" ESCAPE_QUOTES @ONLY)
	else()
		message(FATAL_ERROR "Cannot configure ‘${output-file}’ from ‘${input-file}’.")
	endif()
endif()
