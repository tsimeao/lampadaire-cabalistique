#!/usr/bin/env -S cmake -P
# Copyright © 2019-2020 Thierry Simeao
#
# This file is part of Lampadaire cabalistique.
#
# Lampadaire cabalistique is free software: you can redistribute it and/or modify it under the terms of the GNU Affero
# General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
# option) any later version.
#
# Lampadaire cabalistique is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
# implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License
# for more details.
#
# You should have received a copy of the GNU Affero General Public License along with Lampadaire cabalistique.
# If not, see <https://www.gnu.org/licenses/>.
cmake_minimum_required(VERSION 3.17)

foreach(directory IN ITEMS API/C library GUI)
	file(GLOB meta-files LIST_DIRECTORIES false "${directory}/*.md5" "${directory}/*.map")
	file(REMOVE ${meta-files})
endforeach()
