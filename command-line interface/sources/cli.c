/*
 * Copyright © 2018-2019 Thierry Simeao
 *
 * This file is part of Lampadaire cabalistique.
 *
 * Lampadaire cabalistique is free software: you can redistribute it and/or modify it under the terms of the GNU Affero
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * Lampadaire cabalistique is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License
 * for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with Lampadaire cabalistique.
 * If not, see <https://www.gnu.org/licenses/>.
 */
#include <getopt.h>
#include <locale.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>

#include <lamp_cab.h>

#define USAGE_MESSAGE "Usage: %s [options] [inputs]\n"

// option descriptors
static const struct option long_opts[] = {{"help", no_argument, NULL, 'h'},
                                          {"version", no_argument, NULL, 'V'},
                                          {"verbose", no_argument, NULL, 'v'},
                                          {"outfile", required_argument, NULL, 'o'},
                                          {NULL, 0, NULL, 0}};

static const char short_opts[] = "h"  // help
                                 "V"  // version
                                 "v"  // verbose
                                 "o:" // output file
    ;

int main(int argc, char *argv[]) {
    bool        verbose         = false;
    FILE *      input_file      = stdin;
    FILE *      output_file     = stdout;
    const char *output_filename = "";
    int         result          = EXIT_SUCCESS;
    int         option_code;

    setlocale(LC_ALL, ""); // switch to system locale

    while ((option_code = getopt_long(argc, argv, short_opts, long_opts, NULL)) != -1) {
        switch (option_code) {
        // operation modes:
        case 'h':
            printf("Overview: Lampadaire cabalistique\n"
                   "\n" USAGE_MESSAGE "\nOptions:\n"
                   "\t-h, --help         display this help and exit\n"
                   "\t-V, --version      output version information and exit\n"
                   "\t-o, --outfile=FILE specify output filename\n",
                   argv[0]);
            return EXIT_SUCCESS;

        case 'V':
            printf("Lampadaire cabalistique, version %s (revision #%s)\n"
                   "Flex %s and Bison %s have been used to generate the parser.\n",
                   lamp_cab_version, lamp_cab_revision, flex_version, bison_version);
            return EXIT_SUCCESS;

        // parameters:
        case 'v':
            verbose = true;
            break;

        case 'o':
            if (output_file != stdout) {
                fprintf(stderr, "Warning: the file ‘%s’ will be written instead of the file ‘%s’.\n", optarg,
                        output_filename);
                fclose(output_file);
            }

            if ((output_file = fopen((output_filename = optarg), "w")) == NULL) {
                perror(output_filename);
                return EXIT_FAILURE;
            }
            break;

        default:
            fprintf(stderr, USAGE_MESSAGE "Try ‘%s --help’ for more information.\n", argv[0], argv[0]);
            return EXIT_FAILURE;
        }
    }
    argc -= optind, argv += optind;

    if (argc >= 1) {
        if ((input_file = fopen(argv[0], "r")) == NULL) {
            perror(argv[0]);
            return EXIT_FAILURE;
        }
    }

    result = photo_parse(input_file, output_file, stderr);

    if (input_file != stdin) {
        fclose(input_file), input_file = stdin;
    }

    if (output_file != stdout) {
        fclose(output_file), output_file = stdout;
    }

    return result;
}
