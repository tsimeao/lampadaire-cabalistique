/*
 * Copyright ©2019 Thierry Simeao
 *
 * This file is part of Lampadaire cabalistique.
 *
 * Lampadaire cabalistique is free software: you can redistribute it and/or modify it under the terms of the GNU Affero
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * Lampadaire cabalistique is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License
 * for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with Lampadaire cabalistique.
 * If not, see <https://www.gnu.org/licenses/>.
 */
package io.gitlab.tsimeao.lampadaireCabalistique

import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Lifecycle
import org.junit.Assert
import org.junit.Test

class AppActivityTest : AbstractActivityTest<AppCompatActivity>(AppCompatActivity::class) {
    @Test
    fun activity_canBeInitialized() {
        Assert.assertEquals(Lifecycle.State.INITIALIZED, activity?.lifecycle?.currentState)
    }
}
