/*
 * Copyright ©2019 Thierry Simeao
 *
 * This file is part of Lampadaire cabalistique.
 *
 * Lampadaire cabalistique is free software: you can redistribute it and/or modify it under the terms of the GNU Affero
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * Lampadaire cabalistique is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License
 * for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with Lampadaire cabalistique.
 * If not, see <https://www.gnu.org/licenses/>.
 */
package io.gitlab.tsimeao.lampadaireCabalistique

import android.app.Activity
import androidx.annotation.CallSuper
import org.junit.After
import org.junit.Before
import kotlin.reflect.KClass
import kotlin.reflect.full.createInstance

abstract class AbstractActivityTest<T : Activity>(protected val activityType: KClass<T>) {
    protected var activity: T? = null
        private set

    @Before
    @CallSuper
    open fun prepareTest() {
        try {
            activity = activityType.createInstance()
        } catch (exception: Exception) {
            throw AssertionError("Cannot instantiate an activity of type ‘${activityType.simpleName}’.", exception)
        }
    }

    @After
    @CallSuper
    open fun cleanTest() {
        activity = null
    }
}
