/*
 * Copyright ©2019 Thierry Simeao
 *
 * This file is part of Lampadaire cabalistique.
 *
 * Lampadaire cabalistique is free software: you can redistribute it and/or modify it under the terms of the GNU Affero
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * Lampadaire cabalistique is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License
 * for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with Lampadaire cabalistique.
 * If not, see <https://www.gnu.org/licenses/>.
 */
#include "CodeEditor.hpp"

#include <QColor>
#include <QFont>
#include <QList>
#include <QPainter>
#include <QRectF>
#include <QString>
#include <QTextBlock>
#include <QTextEdit>
#include <QTextFormat>
#include <climits>
#include <cstdint>

#define RE_SPACE_BASIS "(?:[ \t\v]| )"
#define RE_NUMBER_BASIS "(?:0|[1-9](?:(?:_|')?[0-9])*)"
#define RE_KILO_SUFFIX "(?:(?:(?:\\.|,)[0-9])?(?:k|ᵏ))"
#define RE_DECIMAL_SUFFIX "(?:(?:\\.|,)[0-9](?:(?:_|')?[0-9])*)"

#define RE_INTEGER "(?:" RE_NUMBER_BASIS RE_KILO_SUFFIX "?)"
#define RE_DECIMAL "(?:" RE_NUMBER_BASIS "(?:" RE_KILO_SUFFIX "|" RE_DECIMAL_SUFFIX ")?)"
#define RE_SPACE RE_SPACE_BASIS "+"
#define RE_SPACE_OPTIONAL RE_SPACE_BASIS "*"

CodeEditor::CodeEditor() : lineNumberArea(this), highlighter(document()) {
    QFont font("Courier", -1, QFont::Normal, false);
    font.setFixedPitch(true);
    setFont(font);

    connect(this, &QPlainTextEdit::blockCountChanged, this, &CodeEditor::updateLineNumberAreaWidth,
            static_cast<Qt::ConnectionType>(Qt::DirectConnection | Qt::UniqueConnection));
    connect(this, &QPlainTextEdit::cursorPositionChanged, this, &CodeEditor::highlightCurrentLine,
            static_cast<Qt::ConnectionType>(Qt::DirectConnection | Qt::UniqueConnection));
    connect(this, &QPlainTextEdit::updateRequest, this, &CodeEditor::updateLineNumberArea,
            static_cast<Qt::ConnectionType>(Qt::DirectConnection | Qt::UniqueConnection));

    updateLineNumberAreaWidth(blockCount());
    highlightCurrentLine();
}

int CodeEditor::lineNumberAreaWidth(int blockCount) const {
    std::uint_fast8_t nbDigits = 1;

    while (blockCount >= 10) {
        ++nbDigits;
        blockCount /= 10;
    }

    return 3 + fontMetrics().horizontalAdvance('0') * nbDigits;
}

void CodeEditor::highlightCurrentLine() {
    QList<QTextEdit::ExtraSelection> extraSelections;

    if (!isReadOnly()) {
        QTextEdit::ExtraSelection selection;

        selection.format.setBackground(QColor(238, 245, 254));
        selection.format.setProperty(QTextFormat::FullWidthSelection, true);
        selection.cursor = textCursor();
        selection.cursor.clearSelection();

        extraSelections.append(selection);
    }

    setExtraSelections(extraSelections);
}

void CodeEditor::updateLineNumberArea(const QRect &rect, int dy) {
    if (dy) {
        lineNumberArea.scroll(0, dy);
    } else {
        lineNumberArea.update(0, rect.y(), lineNumberArea.width(), rect.height());
    }

    if (rect.contains(viewport()->rect())) {
        updateLineNumberAreaWidth(blockCount());
    }
}

void CodeEditor::updateLineNumberAreaWidth(int blockCount) {
    setViewportMargins(lineNumberAreaWidth(blockCount), 0, 0, 0);
}

void CodeEditor::resizeEvent(QResizeEvent *event) {
    QPlainTextEdit::resizeEvent(event);

    QRect cr = contentsRect();
    cr.setWidth(lineNumberAreaWidth(blockCount()));
    lineNumberArea.setGeometry(cr);
}

QSize CodeEditor::LineNumberArea::sizeHint() const {
    return QSize(editor->lineNumberAreaWidth(editor->blockCount()), 0);
}

void CodeEditor::LineNumberArea::paintEvent(QPaintEvent *event) {
    QPainter  painter(this);
    const int currentLine     = editor->textCursor().blockNumber();
    const int height          = editor->fontMetrics().height();
    const int width           = this->width();
    bool      drawCurrentLine = false;
    int       currentLineTop  = INT_MIN;

    painter.setPen(Qt::lightGray);
    painter.fillRect(event->rect(), Qt::white);

    for (QTextBlock block = editor->firstVisibleBlock(); block.isValid(); block = block.next()) {
        const QRectF blockPosition = editor->blockBoundingGeometry(block).translated(editor->contentOffset());

        if (blockPosition.top() > event->rect().bottom())
            break;

        if (!block.isVisible() || blockPosition.bottom() < event->rect().top())
            continue;

        const int blockNumber = block.blockNumber();
        if (blockNumber == currentLine) {
            drawCurrentLine = true;
            currentLineTop  = blockPosition.top();
            continue;
        }

        painter.drawText(0, blockPosition.top(), width, height, Qt::AlignRight, QString::number(blockNumber + 1));
    }

    if (drawCurrentLine) {
        painter.setPen(Qt::black);
        painter.drawText(0, currentLineTop, width, height, Qt::AlignRight, QString::number(currentLine + 1));
    }
}

CodeEditor::Highlighter::Highlighter(QTextDocument *parent) : QSyntaxHighlighter(parent) {
    QTextCharFormat commentFormat, keywordFormat, numberFormat, exposureParameterFormat, errorFormat;

    commentFormat.setForeground(Qt::lightGray);

    keywordFormat.setFontWeight(QFont::Bold);

    numberFormat.setForeground(Qt::darkCyan);

    exposureParameterFormat.setFontItalic(true);

    errorFormat.setFontUnderline(true);
    errorFormat.setUnderlineColor(Qt::red);
    errorFormat.setUnderlineStyle(QTextCharFormat::SpellCheckUnderline);

    highlightingRules.emplace_back("(?:^|" RE_SPACE ")#.*$", commentFormat);
    highlightingRules.emplace_back("\\b(?:Av?|Aperture|M(?:anual)?|P(?:rogram)?|S(?:peed)?|Tv)\\b", keywordFormat);
    highlightingRules.emplace_back("\\b" RE_DECIMAL "\\b", numberFormat);
    highlightingRules.emplace_back("\\b(?:(?:1|f|𝑓)(?:/|⁄)|F|𝐹)" RE_DECIMAL "\\b", exposureParameterFormat);
    highlightingRules.emplace_back("\\b" RE_DECIMAL RE_SPACE_OPTIONAL "s\\b", exposureParameterFormat);
    highlightingRules.emplace_back("\\b(?:ISO|[Ii]so)" RE_SPACE_OPTIONAL RE_INTEGER "°?\\b", exposureParameterFormat);
    highlightingRules.emplace_back("\\b(?:ASA|[Aa]sa|DIN|[Dd]in)" RE_SPACE_OPTIONAL RE_INTEGER "\\b",
                                   exposureParameterFormat);
    highlightingRules.emplace_back("\\b" RE_INTEGER RE_SPACE_OPTIONAL "(?:ASA|[Aa]sa|DIN|[Dd]in|ISO|[Ii]so)\\b",
                                   exposureParameterFormat);
    highlightingRules.emplace_back("\\b(?:(?:(?:1|f|𝑓)(?:/|⁄)|F|𝐹)|s|ASA|[Aa]sa|DIN|[Dd]in|ISO|[Ii]so|°)\\b",
                                   errorFormat);
}

void CodeEditor::Highlighter::highlightBlock(const QString &text) {
    for (int position = 0, matchedMinimalPosition = INT_MAX, length = 0; true;
         position = matchedMinimalPosition + length, matchedMinimalPosition = INT_MAX, length = 0) {
        const QTextCharFormat *format = nullptr;

        for (const HighlightingRule &rule : highlightingRules) {
            const int matchedPosition = rule.regularExpression.indexIn(text, position, QRegExp::CaretAtZero);

            if (matchedPosition < 0)
                continue;

            const int matchedLength = rule.regularExpression.matchedLength();
            if (matchedMinimalPosition > matchedPosition) {
                matchedMinimalPosition = matchedPosition, length = matchedLength;
                format = &rule.format;
            } else if (matchedMinimalPosition == matchedPosition && length < matchedLength) {
                length = matchedLength;
                format = &rule.format;
            }
        }

        if (length <= 0)
            break;

        setFormat(matchedMinimalPosition, length, *format);
    }
}

CodeEditor::Highlighter::HighlightingRule::HighlightingRule(const char *pattern, const QTextCharFormat &textFormat)
    : regularExpression(pattern, Qt::CaseSensitive, QRegExp::RegExp2), format(textFormat) {}
