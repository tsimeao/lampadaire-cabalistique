/*
 * Copyright © 2019-2020 Thierry Simeao
 *
 * This file is part of Lampadaire cabalistique.
 *
 * Lampadaire cabalistique is free software: you can redistribute it and/or modify it under the terms of the GNU Affero
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * Lampadaire cabalistique is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License
 * for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with Lampadaire cabalistique.
 * If not, see <https://www.gnu.org/licenses/>.
 */
#pragma once
/**
 * @file
 */

#include <QObject>
#include <QPlainTextEdit>
#include <QRect>
#include <QRegExp>
#include <QSyntaxHighlighter>
#include <QTextCharFormat>
#include <QTextDocument>
#include <QWidget>
#include <vector>

/**
 * @brief Text editor highlighting code
 */
class CodeEditor : public QPlainTextEdit {
    Q_OBJECT

public:
    CodeEditor();

private:
    int lineNumberAreaWidth(int blockCount) const;

private slots:
    void highlightCurrentLine();
    void updateLineNumberArea(const QRect &rect, int dy);
    void updateLineNumberAreaWidth(int blockCount);

protected:
    /**
     * @details   Called by Qt when the widget is resized.
     * @param[in] event Qt widget resize event
     */
    void resizeEvent(QResizeEvent *event) override;

private:
    class LineNumberArea final : public QWidget {
    public:
        explicit LineNumberArea(CodeEditor *codeEditor) : QWidget(codeEditor), editor(codeEditor) {}
        QSize sizeHint() const override;

    protected:
        void paintEvent(QPaintEvent *event) override;

    private:
        CodeEditor *const editor;
    } lineNumberArea;

    class Highlighter final : public QSyntaxHighlighter {
    public:
        explicit Highlighter(QTextDocument *parent);

    protected:
        void highlightBlock(const QString &text) override;

    private:
        class HighlightingRule final {
        public:
            HighlightingRule(const char *pattern, const QTextCharFormat &textFormat);

        public:
            const QRegExp         regularExpression;
            const QTextCharFormat format;
        };
        std::vector<HighlightingRule> highlightingRules;
    } highlighter;

    friend class CodeEditor::LineNumberArea;
};
