# Copyright © 2018-2020 Thierry Simeao
#
# This file is part of Lampadaire cabalistique.
#
# Lampadaire cabalistique is free software: you can redistribute it and/or modify it under the terms of the GNU Affero
# General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
# option) any later version.
#
# Lampadaire cabalistique is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
# implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License
# for more details.
#
# You should have received a copy of the GNU Affero General Public License along with Lampadaire cabalistique.
# If not, see <https://www.gnu.org/licenses/>.

stages:
  - build
  - test
  - deploy

variables:
  GIT_SUBMODULE_STRATEGY: none

default:
  before_script:
    - source -- /etc/profile
    - "'continuous integration/Linux/configure.sh'"

Linux:
  image: registry.gitlab.com/tsimeao/lampadaire-cabalistique/linux/desktop:latest
  stage: build
  script:
    - make --directory=_build --no-print-directory -- all
  artifacts:
    name: "${CI_JOB_NAME}"
    paths:
      - _build/library/C/.headers
      - _build/library/C/liblamp-cab.so.*
      - _build/library/C/liblamp-cab.so
      - _build/library/Java/libs/LampJab-*.jar
      - '_build/command-line interface/lampadaire-cabalistique'
      - '_build/graphical user interface/Qt/lampadaire-cabalistique-gui'
    expire_in: 3 hrs

android:
  image: registry.gitlab.com/tsimeao/lampadaire-cabalistique/linux/android:latest
  stage: build
  script:
    - ninja -C _build -- android
  artifacts:
    name: "${CI_JOB_NAME}"
    paths:
      - '_build/graphical user interface/android/outputs/apk/release'
    expire_in: 3 hrs

.offline:
  stage: test
  artifacts:
    name: "${CI_JOB_NAME}"
    paths:
      - _build/*/*/reports/tests/test*
    reports:
      junit:
        - _build/library/C/TEST-*.xml
        - _build/*/*/test-results/test*/TEST-*.xml
    when: on_failure
    expire_in: 1 week

Linux:offline:
  extends: .offline
  needs:
    - job: Linux
      artifacts: false
  image: registry.gitlab.com/tsimeao/lampadaire-cabalistique/linux/desktop:latest
  variables:
    GIT_SUBMODULE_STRATEGY: normal
  script:
    - cd -- _build
    - make --no-print-directory -- unit-tests
    - ctest --output-on-failure --no-label-summary --label-regex '^desktop$'

android:offline:
  extends: .offline
  needs:
    - job: android
      artifacts: false
  image: registry.gitlab.com/tsimeao/lampadaire-cabalistique/linux/android:latest
  script:
    - cd -- _build
    - ctest --output-on-failure --no-label-summary --label-regex '^android$'

pages:
  image: registry.gitlab.com/tsimeao/lampadaire-cabalistique/linux/documentation:latest
  stage: deploy
  dependencies: [ ]
  script:
    - ninja -C _build -- html
  artifacts:
    name: 'code_documentation'
    paths:
      - public
  rules:
    - if: '$CI_COMMIT_BRANCH == "master" && $CI_PROJECT_PATH == "tsimeao/lampadaire-cabalistique"'
