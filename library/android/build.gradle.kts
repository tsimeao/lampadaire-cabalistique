/*
 * Copyright © 2019-2020 Thierry Simeao
 *
 * This file is part of Lampadaire cabalistique.
 *
 * Lampadaire cabalistique is free software: you can redistribute it and/or modify it under the terms of the GNU Affero
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * Lampadaire cabalistique is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License
 * for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with Lampadaire cabalistique.
 * If not, see <https://www.gnu.org/licenses/>.
 */
plugins {
    id("com.android.library")
    kotlin("android")
    id("kotlin-android-extensions")
}
android {
    ndkVersion = properties["version.android.ndk"] as String
    compileSdkVersion((properties["version.android.sdk"] as String).toInt())
    buildToolsVersion = properties["version.android.build.tools"] as String
    defaultConfig {
        minSdkVersion(properties["version.android.sdk.minimum"] as String)
        targetSdkVersion(properties["version.android.sdk"] as String)
        versionCode = (properties["version.code"] as String).toInt()
        versionName = project.version as String
        testInstrumentationRunner = "androidx.test.runner.AndroidJUnitRunner"

        externalNativeBuild!!.cmake {
            targets.add("lamp-cab")
            arguments.add("-DPROJECT_VERSION:INTERNAL=${project.version}")
        }
    }
    compileOptions {
        sourceCompatibility = JavaVersion.VERSION_1_8
        targetCompatibility = JavaVersion.VERSION_1_8
        coreLibraryDesugaringEnabled = true
    }
    buildTypes {
        getByName("debug") {
            ndk {
                abiFilters("x86", "armeabi-v7a")
            }

            isDebuggable = true
            isJniDebuggable = true

            isPseudoLocalesEnabled = true
        }
        getByName("release") {
            isDebuggable = false
            isJniDebuggable = false

            isMinifyEnabled = false
            setProguardFiles(listOf(getDefaultProguardFile("proguard-android-optimize.txt")))
        }
    }
    sourceSets.all {
        java.srcDir("src/${name}/kotlin")
    }
    externalNativeBuild.cmake {
        version = properties["version.CMake"] as String
        path = projectDir.resolve("../C/CMakeLists.txt")
        buildStagingDirectory = buildDir
    }
}
dependencies {
    implementation(kotlin("stdlib-jdk8", properties["version.kotlin"] as String))
    implementation("androidx.core:core-ktx:${properties["version.android.core"]}")
    implementation("androidx.appcompat:appcompat:${properties["version.android.appcompat"]}")
    implementation(fileTree("dir" to "libs", "include" to "*.jar"))
    coreLibraryDesugaring("com.android.tools:desugar_jdk_libs:${properties["version.android.tools.desugarJdkLibs"]}")

    androidTestImplementation("androidx.test:runner:${properties["version.android.test.runner"]}")
    androidTestImplementation("androidx.test.espresso:espresso-core:${properties["version.android.test.espresso"]}")

    testImplementation(kotlin("reflect", properties["version.kotlin"] as String))
    testImplementation(kotlin("test", properties["version.kotlin"] as String))
    testImplementation(kotlin("test-junit", properties["version.kotlin"] as String))
}
