/*
 * Copyright © 2018-2020 Thierry Simeao
 *
 * This file is part of Lampadaire cabalistique.
 *
 * Lampadaire cabalistique is free software: you can redistribute it and/or modify it under the terms of the GNU Affero
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * Lampadaire cabalistique is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License
 * for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with Lampadaire cabalistique.
 * If not, see <https://www.gnu.org/licenses/>.
 */
#pragma once
/**
 * @file
 */

#include <ostream>

namespace Parser {

/**
 * @brief Textual location of an item
 */
class Location {
public:
    /**
     * @brief Item edge
     */
    class Edge {
    public:
        int line; ///< Line number
        struct {
            int character; ///< Position of the Unicode® code point in the line
            int position;  ///< Position of the Unicode® code point in the line taking into account tabs
        } column;          ///< Column numbers

        /**
         * @param[in] initialColumn number of the first column
         */
        constexpr explicit Edge(int initialColumn) noexcept : line(1), column({initialColumn, initialColumn}) {}
    } begin, ///< Beginning of the item
        end; ///< End of the item

    constexpr Location() noexcept : begin(1), end(1) {}
};

/**
 * @brief      Print an error message
 * @param[out] output      output stream
 * @param[in]  location    location of the error
 * @param[in]  message     text to print
 * @param[in]  interactive print in TTY mode
 * @param[in]  color       use coloration
 */
void printError(std::ostream &output, const Location &location, const std::string &message, bool interactive,
                bool color);

} // namespace Parser

/**
 * @brief      Generate a string from \a location on the \a output stream
 * @relates    Parser::Location
 * @param[out] output   output stream
 * @param[in]  location location to display
 * @return     the \c std::ostream object
 */
std::ostream &operator<<(std::ostream &output, const Parser::Location &location);
