/*
 * Copyright ©2020 Thierry Simeao
 *
 * This file is part of Lampadaire cabalistique.
 *
 * Lampadaire cabalistique is free software: you can redistribute it and/or modify it under the terms of the GNU Affero
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * Lampadaire cabalistique is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License
 * for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with Lampadaire cabalistique.
 * If not, see <https://www.gnu.org/licenses/>.
 */
#pragma once
/**
 * @file
 */

#include <type_traits>

namespace Utils {

/**
 * @tparam Minimum lower bound
 * @tparam Maximum upper bound
 * @return Is the relation \f$ number \in [Minimum, Maximum] \f$ verified?
 *
 * @details Here is an example of how to use:
 * @code
 * // Utils::isInRange<Minimum, Maximum>(value)
 * if (int theAnswer = 42; Utils::isInRange<1, 337>(theAnswer)) {
 *     std::cout << theAnswer << " ∈ [1, 337]\n";
 * }
 * @endcode
 */
template <int Minimum, int Maximum, typename T> inline constexpr bool isInRange(T number) noexcept {
    static_assert(Minimum <= Maximum);
    static_assert(std::is_integral_v<T>);

    return static_cast<std::make_unsigned_t<T>>(number - Minimum) <= static_cast<unsigned int>(Maximum - Minimum);
}

} // namespace Utils
