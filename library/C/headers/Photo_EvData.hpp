/*
 * Copyright © 2018-2020 Thierry Simeao
 *
 * This file is part of Lampadaire cabalistique.
 *
 * Lampadaire cabalistique is free software: you can redistribute it and/or modify it under the terms of the GNU Affero
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * Lampadaire cabalistique is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License
 * for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with Lampadaire cabalistique.
 * If not, see <https://www.gnu.org/licenses/>.
 */
#pragma once
/**
 * @file
 */

#include <cmath>
#include <cstdint>
#include <optional>
#include <ostream>
#include <string>
#include <type_traits>

namespace Photo {

/**
 * @brief   Aggregate of all exposure data
 * @details All its values are in sixth of stops unit.
 * @note    Data format refers to the APEX (<b>A</b>dditive System of <b>P</b>hotographic <b>Ex</b>posure) unit system.
 */
class EvData final {
public:
    /**
     * @brief  6th of stops.
     * @tparam StopSixthFinal type of stop
     * @note   This type includes the special value \c StopSixth::UNDEFINED which uses overflow to permit:
     *         \f$ + \text{StopSixth::UNDEFINED} = - \text{StopSixth::UNDEFINED} \f$.
     */
    template <class StopSixthFinal> class StopSixth {
    public:
        typedef std::int16_t IntegerType; ///< Internal integer type
        /**
         * @brief Default value
         * @note  It allows: \f$ + \text{UNDEFINED} = - \text{UNDEFINED} \f$.
         */
        static constexpr IntegerType UNDEFINED = INT16_MIN;

    protected:
        IntegerType value; ///< Internal value

        /**
         * @param[in] value internal value
         */
        constexpr explicit StopSixth(IntegerType value) noexcept : value(value) {
            static_assert(std::is_base_of_v<StopSixth, StopSixthFinal> && std::is_final_v<StopSixthFinal>);
        }
        /**
         * @param[in] other 6th of stops to copy
         */
        constexpr StopSixth(const StopSixth &other) noexcept : value(other.value) {
            static_assert(std::is_base_of_v<StopSixth, StopSixthFinal> && std::is_final_v<StopSixthFinal>);
        }
        /**
         * @param[in] other 6th of stops to copy
         */
        constexpr StopSixth &operator=(const StopSixth &other) = default;

    public:
        virtual ~StopSixth() {}

        /**
         * @return Is the value filled?
         */
        explicit operator bool() const noexcept { return UNDEFINED != value; }
        /**
         * @return Is the value empty?
         */
        bool operator!() const noexcept { return UNDEFINED == value; }
        /**
         * @return internal value
         */
        operator IntegerType() const noexcept { return value; }
        /**
         * @note Always returns \c false if one of the two values is \c UNDEFINED.
         */
        bool operator==(const StopSixthFinal &other) const noexcept {
            return static_cast<bool>(*this) && value == other.value;
        }
        /**
         * @note Always returns \c true if one of the two values is \c UNDEFINED.
         */
        bool operator!=(const StopSixthFinal &other) const noexcept { return !*this || value != other.value; }
        /**
         * @note Always returns \c false if one of the two values is \c UNDEFINED.
         */
        virtual bool operator>(const StopSixthFinal &other) const {
            return static_cast<bool>(other) && value > other.value;
        }
        /**
         * @note Always returns \c false if one of the two values is \c UNDEFINED.
         */
        virtual bool operator<(const StopSixthFinal &other) const {
            return static_cast<bool>(*this) && value < other.value;
        }
        /**
         * @note Always returns \c false if one of the two values is \c UNDEFINED.
         */
        virtual bool operator>=(const StopSixthFinal &other) const {
            return static_cast<bool>(other) && value >= other.value;
        }
        /**
         * @note Always returns \c false if one of the two values is \c UNDEFINED.
         */
        virtual bool operator<=(const StopSixthFinal &other) const {
            return static_cast<bool>(*this) && value <= other.value;
        }
    };

protected:
    /**
     * @brief   Inverted 6th of stops
     * @details The more is the internal value, the less is the exposure.
     * @tparam  StopSixthFinal type of stop
     */
    template <class StopSixthFinal> class InvertedStopSixth : public StopSixth<StopSixthFinal> {
    protected:
        /**
         * @param[in] value internal value
         */
        constexpr explicit InvertedStopSixth(typename StopSixth<StopSixthFinal>::IntegerType value) noexcept
            : StopSixth<StopSixthFinal>(value) {
            static_assert(std::is_base_of_v<InvertedStopSixth, StopSixthFinal>);
        }
        /**
         * @param[in] other 6th of stops to copy
         */
        constexpr InvertedStopSixth(const InvertedStopSixth &other) noexcept : StopSixth<StopSixthFinal>(other) {
            static_assert(std::is_base_of_v<InvertedStopSixth, StopSixthFinal>);
        }
        /**
         * @param[in] other 6th of stops to copy
         */
        constexpr InvertedStopSixth &operator=(const InvertedStopSixth &other) = default;

    public:
        /**
         * @details Comparison is made in the direction of the light uptake, not the internal value.
         * @note    Always returns \c false if one of the two values is \c UNDEFINED.
         */
        bool operator>(const StopSixthFinal &other) const override {
            return StopSixth<StopSixthFinal>::operator<(other);
        }
        /**
         * @details Comparison is made in the direction of the light uptake, not the internal value.
         * @note    Always returns \c false if one of the two values is \c UNDEFINED.
         */
        bool operator<(const StopSixthFinal &other) const override {
            return StopSixth<StopSixthFinal>::operator>(other);
        }
        /**
         * @details Comparison is made in the direction of the light uptake, not the internal value.
         * @note    Always returns \c false if one of the two values is \c UNDEFINED.
         */
        bool operator>=(const StopSixthFinal &other) const override {
            return StopSixth<StopSixthFinal>::operator<=(other);
        }
        /**
         * @details Comparison is made in the direction of the light uptake, not the internal value.
         * @note    Always returns \c false if one of the two values is \c UNDEFINED.
         */
        bool operator<=(const StopSixthFinal &other) const override {
            return StopSixth<StopSixthFinal>::operator>=(other);
        }
    };

public:
    /**
     * @brief Exposure value
     */
    class Ev final : public StopSixth<Ev> {
    public:
        /**
         * @param[in] value internal value
         */
        constexpr explicit Ev(IntegerType value = 0) noexcept : StopSixth(value) {}
    };
    /**
     * @brief   %Aperture.
     * @details It is calculated as follows: \f$ \frac{Av}{6} = 2 \times \log_2(f\text{-number}) \f$.
     */
    class Aperture final : public InvertedStopSixth<Aperture> {
    public:
        /**
         * @param[in] value internal value
         */
        constexpr explicit Aperture(IntegerType value = UNDEFINED) noexcept : InvertedStopSixth(value) {}

        /**
         * @brief   Upper bound
         * @details It's worth \f$ \lfloor 6 \times 2 \times \log_2(2^{31} - 1) \rfloor = +371 \f$.
         */
        static constexpr IntegerType MAXIMUM = 371;
        /**
         * @brief   Lower bound
         * @details It's worth \f$ -68 \equiv f/0.02 \f$.
         */
        static constexpr IntegerType MINIMUM = -68;

        /**
         * @brief  Convert aperture from 6th of stops to <em>f</em>-number
         * @return aperture in \f$ f\text{-number} \f$ if set
         */
        std::optional<double> toFNumber() const;

        /**
         * @return aperture as a \c std::wstring
         */
        operator std::wstring() const;
    };
    /**
     * @brief   Shutter speed
     * @details It is calculated as follows: \f$ \frac{Tv}{6} = - \log_2(\text{exposure time}) \f$.
     */
    class ShutterSpeed final : public InvertedStopSixth<ShutterSpeed> {
    public:
        /**
         * @param[in] value internal value
         */
        constexpr explicit ShutterSpeed(IntegerType value = UNDEFINED) noexcept : InvertedStopSixth(value) {}

        /**
         * @brief   Upper bound
         * @details It's worth \f$ \lfloor 6 \times \log_2(2^{31} - 1) \rfloor = +185 \f$.
         */
        static constexpr IntegerType MAXIMUM = 185;
        /**
         * @brief   Lower bound
         * @details It's worth \f$ \lceil 6 \times - \log_2(2^{31} - 1) \rceil = -185 \f$.
         */
        static constexpr IntegerType MINIMUM = -185;

        /**
         * @brief  Convert shutter speed from 6th of stops to seconds
         * @return exposure time in \f$ s \f$ if set
         */
        std::optional<double> toSeconds() const;
        /**
         * @brief  Convert shutter speed from 6th of stops to reverse seconds
         * @return shutter speed in \f$ s^{-1} \f$ if set
         */
        std::optional<double> toReverseSeconds() const;

        /**
         * @return shutter speed as a \c std::wstring
         */
        operator std::wstring() const;
    };
    /**
     * @brief   %Sensitivity.
     * @details It is calculated as follows: \f$ \frac{Sv}{6} = \log_2(\frac{ISO\text{-number}}{3.125}) \f$.
     * @note    It is the variant of the Exif standard. APEX originally defined the sensitivity as follows:
     *          \f$ S_v = \log_2(ISO\text{-number} \times 2^{-\frac{7}{4}}) \f$.
     */
    class Sensitivity final : public StopSixth<Sensitivity> {
    public:
        /**
         * @param[in] value internal value
         */
        constexpr explicit Sensitivity(IntegerType value = UNDEFINED) noexcept : StopSixth(value) {}

        /**
         * @brief   Upper bound
         * @details It's worth \f$ \lfloor 6 \times \log_2(\frac{2^{31} - 1}{3.125}) \rfloor = +176 \f$.
         */
        static constexpr IntegerType MAXIMUM = 176;
        /**
         * @brief   Lower bound
         * @details It's worth \f$ -10 \approx 6 \times \log_2(\frac{1}{3.125}) \f$.
         */
        static constexpr IntegerType MINIMUM = -10;

        /**
         * @brief  Convert sensitivity from 6th of stops to ISO
         * @return sensitivity in <em>ISO</em> or <em>ASA</em> if set
         */
        std::optional<std::int_fast32_t> toIso() const;
        /**
         * @brief  Convert sensitivity from 6th of stops to DIN
         * @return sensitivity in <em>DIN</em> if set
         */
        std::optional<float> toDin() const;

        /**
         * @return sensitivity as a \c std::wstring
         */
        operator std::wstring() const;
    };

    float        Bv;   ///< Brightness
    Aperture     Av;   ///< %Aperture.
    ShutterSpeed Tv;   ///< Shutter speed
    Sensitivity  Sv;   ///< %Sensitivity.
    Ev           bias; ///< Exposure bias

    constexpr EvData() noexcept : Bv(NAN) {}
};

/**
 * @details    Generate a string from \a aperture on the \a output stream.
 * @relates    EvData::Aperture
 * @param[out] output   output stream
 * @param[in]  aperture aperture to display
 * @return     the \c std::wostream object
 */
inline std::wostream &operator<<(std::wostream &output, const EvData::Aperture aperture) {
    return output << static_cast<std::wstring>(aperture);
}
/**
 * @details    Generate a string from \a shutterSpeed on the \a output stream.
 * @relates    EvData::ShutterSpeed
 * @param[out] output       output stream
 * @param[in]  shutterSpeed shutter speed to display
 * @return     the \c std::wostream object
 */
inline std::wostream &operator<<(std::wostream &output, const EvData::ShutterSpeed shutterSpeed) {
    return output << static_cast<std::wstring>(shutterSpeed);
}
/**
 * @details    Generate a string from \a sensitivity on the \a output stream.
 * @relates    EvData::Sensitivity
 * @param[out] output      output stream
 * @param[in]  sensitivity sensitivity to display
 * @return     the \c std::wostream object
 */
std::wostream &operator<<(std::wostream &output, const EvData::Sensitivity sensitivity);

} // namespace Photo
