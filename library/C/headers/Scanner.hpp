/*
 * Copyright © 2018-2019 Thierry Simeao
 *
 * This file is part of Lampadaire cabalistique.
 *
 * Lampadaire cabalistique is free software: you can redistribute it and/or modify it under the terms of the GNU Affero
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * Lampadaire cabalistique is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License
 * for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with Lampadaire cabalistique.
 * If not, see <https://www.gnu.org/licenses/>.
 */
#pragma once
/**
 * @file
 */

#include <cstdint>
#include <memory>

#include <LampCab/InputStream.hpp>
#include <LampCab/OutputStream.hpp>

#include "Parser.hpp"

/**
 * @brief Base class of a scanner
 */
class Scanner {
protected:
    std::unique_ptr<LampCab::InputStream>  in;             ///< Parsed input stream
    std::shared_ptr<LampCab::OutputStream> out;            ///< Output stream
    Parser::Location::Edge                 positionInFile; ///< Parsing cursor
    std::uint_fast8_t                      tabStop;        ///< Distance between tab stops

    /**
     * @param[in,out] inputStream  input stream to parse
     * @param[in,out] outputStream stream where to print
     */
    Scanner(std::unique_ptr<LampCab::InputStream> &&      inputStream,
            const std::shared_ptr<LampCab::OutputStream> &outputStream) noexcept;

public:
    virtual ~Scanner() {}

    Scanner(const Scanner &) = delete;
    Scanner &operator=(const Scanner &) = delete;

    /**
     * @return parsed input stream
     */
    LampCab::InputStream &getIn() { return *in; }
    /**
     * @return output stream
     */
    LampCab::OutputStream &getOut() { return *out; }

protected:
    /**
     * @brief      Get the position of the token in the parsed file
     * @param[out] location location of the token
     * @param[in]  text     content of the token
     * @param[in]  length   size of the token
     */
    void updateLocation(Parser::Location &location, const char *text, int length) noexcept;
    /**
     * @brief         Adjust the \a location for an end of line
     * @param[in,out] location location of the end of line
     */
    void updateLocationForEndOfLine(Parser::Location &location) noexcept;

public:
    /**
     * @brief     Convert \a text to \c std::int_fast32_t
     * @param[in] text              token to read
     * @param[in] length            size of the token
     * @param[in] multiplier        multiplier to apply to the result
     * @exception std::out_of_range read value is out of the range of a representable value
     * @return    converted integer number
     * @warning   The \a multiplier must be a power of \c 10.
     */
    static std::int_fast32_t readInteger(const char *text, int length, int multiplier = 1);
    /**
     * @brief     Convert \a text to \c double
     * @param[in] text                  token to read
     * @param[in] length                size of the token
     * @exception std::out_of_range     read value is out of the range of a representable value
     * @exception std::invalid_argument conversion can't be performed
     * @return    converted floating-point number
     */
    static double readDecimal(const char *text, int length);
};
