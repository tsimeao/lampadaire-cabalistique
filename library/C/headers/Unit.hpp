/*
 * Copyright © 2018-2020 Thierry Simeao
 *
 * This file is part of Lampadaire cabalistique.
 *
 * Lampadaire cabalistique is free software: you can redistribute it and/or modify it under the terms of the GNU Affero
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * Lampadaire cabalistique is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License
 * for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with Lampadaire cabalistique.
 * If not, see <https://www.gnu.org/licenses/>.
 */
#pragma once
/**
 * @file
 */

#include <cstdint>
#include <locale>

#include "Photo_EvData.hpp"

namespace Unit {

/**
 * @details \f$ \sqrt{2} \f$
 */
inline constexpr long double PRINCIPAL_SQUARE_ROOT_OF_2 = 0x1.6A09'E667'F3BC'C908p0L;
/**
 * @details \f$ \sqrt[3]{2} \f$
 */
inline constexpr long double PRINCIPAL_CUBE_ROOT_OF_2 = 0x1.428A'2F98'D728'AE22p0L;
/**
 * @details \f$ \sqrt[4]{2} \f$
 */
inline constexpr long double PRINCIPAL_4TH_ROOT_OF_2 = 0x1.306F'E0A3'1B71'52DEp0L;
/**
 * @details \f$ \sqrt[6]{2} \f$
 */
inline constexpr long double PRINCIPAL_6TH_ROOT_OF_2 = 0x1.1F59'AC3C'7D6B'FD6Cp0L;
/**
 * @details \f$ 2^{\frac{2}{3}} \f$
 */
inline constexpr long double TWO_RAISED_TO_THE_POWER_OF_2_3RD = 0x1.965F'EA53'D6E3'C82Cp0L;
/**
 * @details \f$ 2^{\frac{3}{4}} \f$
 */
inline constexpr long double TWO_RAISED_TO_THE_POWER_OF_3_4TH = 0x1.AE89'F995'AD3A'D5E8p0L;
/**
 * @details \f$ 2^{\frac{5}{6}} \f$
 */
inline constexpr long double TWO_RAISED_TO_THE_POWER_OF_5_6TH = 0x1.C823'E074'EC12'946Ap0L;

/**
 * @return formatting of the numbers
 */
std::locale locale();

/**
 * @brief       Convert aperture from <em>f</em>-number to 6th of stops
 * @relatesalso Photo::EvData::Aperture
 * @param[out]  Av          aperture in 6th of stops
 * @param[in]   fNumber     aperture in \f$ f\text{-number} \f$
 * @param[in]   roundToHalf round to half stops instead of 6th of stops
 * @return      overflow
 */
bool apertureFromFNumber(Photo::EvData::Aperture &Av, float fNumber, bool roundToHalf = false);
/**
 * @brief       Convert shutter speed from seconds to 6th of stops
 * @details     It is implemented as \f$ \frac{Tv}{6} = - \frac{10}{3} \times \log_{10}(time) \approx - \log_2(time) \f$
 *              to accept standardized values (<em>exempli gratia</em> 30 s instead of 32 s).
 * @relatesalso Photo::EvData::ShutterSpeed
 * @param[out]  Tv          shutter speed in 6th of stops
 * @param[in]   time        exposure time in \f$ s \f$
 * @param[in]   roundToHalf round to half stops instead of 6th of stops
 * @return      overflow
 */
bool shutterSpeedFromSeconds(Photo::EvData::ShutterSpeed &Tv, float time, bool roundToHalf = false);
/**
 * @brief       Convert shutter speed from reverse seconds to 6th of stops
 * @details     It is implemented as \f$ \frac{Tv}{6} = \frac{10}{3} \times \log_{10}(speed) \approx \log_2(speed) \f$
 *              to accept standardized values (<em>exempli gratia</em> 1/60 s instead of 1/64 s or 1/125 s for 1/128 s).
 * @relatesalso Photo::EvData::ShutterSpeed
 * @param[out]  Tv          shutter speed in 6th of stops
 * @param[in]   speed       shutter speed in \f$ s^{-1} \f$
 * @param[in]   roundToHalf round to half stops instead of 6th of stops
 * @return      overflow
 */
bool shutterSpeedFromReverseSeconds(Photo::EvData::ShutterSpeed &Tv, float speed, bool roundToHalf = false);
/**
 * @brief       Convert sensitivity from ISO unit to 6th of stops
 * @relatesalso Photo::EvData::Sensitivity
 * @param[out]  Sv          sensitivity in 6th of stops
 * @param[in]   iso         sensitivity in <em>ISO</em> or <em>ASA</em>
 * @param[in]   roundToHalf round to half stops instead of 6th of stops
 * @return      overflow
 */
bool sensitivityFromIso(Photo::EvData::Sensitivity &Sv, std::int_fast32_t iso, bool roundToHalf = false);
/**
 * @brief       Convert sensitivity from DIN unit to 6th of stops
 * @relatesalso Photo::EvData::Sensitivity
 * @param[out]  Sv      sensitivity in 6th of stops
 * @param[in]   degrees sensitivity in <em>DIN</em>
 * @return      overflow
 */
bool sensitivityFromDin(Photo::EvData::Sensitivity &Sv, std::int_fast32_t degrees);

} // namespace Unit
