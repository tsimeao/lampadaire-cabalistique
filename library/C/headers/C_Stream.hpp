/*
 * Copyright © 2018-2019 Thierry Simeao
 *
 * This file is part of Lampadaire cabalistique.
 *
 * Lampadaire cabalistique is free software: you can redistribute it and/or modify it under the terms of the GNU Affero
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * Lampadaire cabalistique is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License
 * for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with Lampadaire cabalistique.
 * If not, see <https://www.gnu.org/licenses/>.
 */
#pragma once
/**
 * @file
 */

#include <cstdio>
#include <ios>
#include <ostream>
#include <streambuf>

#include <LampCab/InputStream.hpp>
#include <LampCab/OutputStream.hpp>
#include <LampCab/Stream.hpp>

namespace C {

/**
 * @brief C implementation of LampCab::Stream, LampCab::InputStream, \& LampCab::OutputStream
 */
class Stream final : public LampCab::InputStream, public LampCab::OutputStream {
protected:
    /**
     * @brief Interface with the C standard library
     */
    class Buffer final : public std::streambuf {
    private:
        Stream &         streamLampCab; ///< External stream
        std::FILE *const streamC;       ///< Internal C stream

    public:
        /**
         * @param[in] streamLampCab object which encapsulates this C::Stream::Buffer
         * @param[in] streamC       C stream to manage
         */
        Buffer(Stream &streamLampCab, std::FILE *streamC) : streamLampCab(streamLampCab), streamC(streamC) {}
        ~Buffer() override;

        /**
         * @return the associated stream has no errors
         */
        explicit operator bool() const noexcept { return !std::ferror(streamC); }
        /**
         * @return an error has occurred on the associated stream
         */
        bool operator!() const noexcept { return std::ferror(streamC); }

    protected:
        /**
         * @brief      Read up to \a lengthMax bytes from the input stream into \a buffer
         * @details    It blocks until input data is available or an end of stream (or end of line if
         *             \a LampCab::InputStream::isInteractive) is detected.
         * @param[out] buffer    buffer into which the data is read
         * @param[in]  lengthMax maximum number of bytes to read
         * @return     number of bytes read
         */
        std::streamsize xsgetn(char *buffer, std::streamsize lengthMax) override;
        /**
         * @brief     Write \a length bytes from \a buffer to this output stream
         * @param[in] buffer buffer containing the data to write
         * @param[in] length number of bytes to write
         * @return    number of bytes written
         */
        std::streamsize xsputn(const char *buffer, std::streamsize length) override;
        /**
         * @brief  Synchronize stream buffer
         * @return \c 0 on success, \c -1 on failure
         */
        int sync() override;
    } streamBuffer;            ///< C stream interface
    std::ostream outputStream; ///< Associated standard library stream

public:
    /**
     * @param[in] file C stream to manage
     * @param[in] tty  the stream is a TTY
     */
    Stream(std::FILE *file, bool tty)
        : LampCab::Stream(tty), LampCab::InputStream(tty), LampCab::OutputStream(tty), streamBuffer(*this, file),
          outputStream(&streamBuffer) {}
    /**
     * @details   Create an interactive stream with no TTY capabilities (no colorization).
     * @param[in] file C stream to manage
     */
    Stream(std::FILE *file, std::nullptr_t)
        : LampCab::Stream(false), LampCab::InputStream(true), LampCab::OutputStream(false), streamBuffer(*this, file),
          outputStream(&streamBuffer) {}
    /**
     * @details   Create a stream with automatically determined TTY capabilities.
     * @param[in] file C stream to manage
     */
    explicit Stream(std::FILE *file);
    Stream(const Stream &) = delete;
    Stream &operator=(const Stream &) = delete;

    std::streamsize read(char *buffer, std::streamsize lengthMax) override;
                    operator std::ostream &() override;
    explicit        operator bool() const override;
    bool            operator!() const override;
};

} // namespace C
