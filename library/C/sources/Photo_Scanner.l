/*
 * Copyright © 2018-2019 Thierry Simeao
 *
 * This file is part of Lampadaire cabalistique.
 *
 * Lampadaire cabalistique is free software: you can redistribute it and/or modify it under the terms of the GNU Affero
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * Lampadaire cabalistique is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License
 * for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with Lampadaire cabalistique.
 * If not, see <https://www.gnu.org/licenses/>.
 */
/* clang-format off */
%option 8bit reentrant
%option extra-type="Photo::Scanner*"

%pointer
%option nodefault
%option noinput nounput noyywrap

%{
// clang-format on
#include "Photo_Scanner.hpp"

#include "Photo_Parser.hpp"

#include <lamp_cab.h>

#include <cfloat>
#include <new>
#include <stdexcept>
#include <utility>

#define YY_INPUT(buffer, result, maxSize)                        \
    if ((result = yyextra->getIn().read(buffer, maxSize)) < 0) { \
        result = 0;                                              \
        YY_FATAL_ERROR("input in photo scanner failed");         \
    }

#define YY_DECL \
    template <> \
    Photo::Parser::symbol_type Photo::Scanner::lex<Photo::Parser::symbol_type>(::Parser::Location &&location)

#define YY_USER_ACTION \
    yy_set_bol(0);     \
    updateLocation(location, yytext, yyleng);

// clang-format off
%}

integer     0|[1-9]((_|')?[0-9])*
decimal     {integer}(\.|,)[0-9]((_|')?[0-9])*
slash       \/|⁄

%% /* Rules */
    /* separators */
\r?\n|\r {
    // clang-format on
    updateLocationForEndOfLine(location);
    yy_set_bol(1);
    return Photo::Parser::symbol_type('\n', location);
    // clang-format off
}

;                   return Photo::Parser::symbol_type(';', location);

    /* spaces */
[ \t\v]+            return Photo::Parser::symbol_type(' ', location);

#[^\r\n]*           /* comment */

    /* suffixes */
s                   return Photo::Parser::symbol_type('s', location);
°                   return Photo::Parser::make_DEGREE_SIGN(location);

    /* aperture */
(f|𝑓){slash}        |
F|𝐹                 return Photo::Parser::make_APERTURE_PREFIX(location);

    /* shutter speed */
1{slash}            return Photo::Parser::make_SHUTTER_SPEED_PREFIX(location);

    /* sensitivity */
ISO|[Ii]so          return Photo::Parser::make_ISO(location);
ASA|[Aa]sa          return Photo::Parser::make_ASA(location);
DIN|[Dd]in          return Photo::Parser::make_DIN(location);

    /* AE modes */
P(rogram)?          return Photo::Parser::symbol_type('P', location);
S(peed)?|Tv         return Photo::Parser::symbol_type('S', location);
Av?|Aperture        return Photo::Parser::symbol_type('A', location);
M(anual)?           return Photo::Parser::symbol_type('M', location);

    /* numbers */
{integer} {
    // clang-format on
    try {
        return Photo::Parser::make_INTEGER(Scanner::readInteger(yytext, yyleng), location);
    } catch (const std::out_of_range &oor) {
        return Photo::Parser::symbol_type('#', DBL_MAX, location); // too long number
    }
    // clang-format off
}

{integer}((\.|,)[0-9])?(k|ᵏ) {
    // clang-format on
    try {
        return Photo::Parser::make_INTEGER(Scanner::readInteger(yytext, yyleng, 1'000 /* “kilo” suffix */), location);
    } catch (const std::out_of_range &oor) {
        return Photo::Parser::symbol_type('#', DBL_MAX, location); // too long number
    }
    // clang-format off
}

{decimal} {
    // clang-format on
    try {
        return Photo::Parser::make_DECIMAL(Scanner::readDecimal(yytext, yyleng), location);
    } catch (const std::out_of_range &oor) {
        return Photo::Parser::symbol_type('#', DBL_MAX, location); // too long number
    }
    // clang-format off
}

<<EOF>>         return Photo::Parser::make_END(location);

    /* default rule */
[^# ;\t\v\r\n]  return Photo::Parser::symbol_type('\a', location); // alert

%% /* Additional Code */
// clang-format on

Photo::Scanner::Scanner(std::unique_ptr<LampCab::InputStream> &&      inputStream,
                        const std::shared_ptr<LampCab::OutputStream> &outputStream)
    : ::Scanner(std::move(inputStream), outputStream) {
    if (yylex_init_extra(this, &yyscanner) != 0) {
        throw std::bad_alloc();
    }
}

Photo::Scanner::~Scanner() { yylex_destroy(yyscanner); }
