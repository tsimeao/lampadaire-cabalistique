/*
 * Copyright © 2018-2020 Thierry Simeao
 *
 * This file is part of Lampadaire cabalistique.
 *
 * Lampadaire cabalistique is free software: you can redistribute it and/or modify it under the terms of the GNU Affero
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * Lampadaire cabalistique is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License
 * for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with Lampadaire cabalistique.
 * If not, see <https://www.gnu.org/licenses/>.
 */
// clang-format off
%require "3.4"

// options
%language       "c++"
%define lr.type lalr
%locations
%define api.namespace     {Photo}
%define api.parser.class  {Parser}
%define api.location.type {::Parser::Location}
%define api.value.type    variant
%define api.token.constructor
%define parse.error verbose

// internal interfaces of the parser
%param       {std::shared_ptr<Photo::Scanner>        scanner}
%parse-param {std::shared_ptr<LampCab::OutputStream> errorStream}

// environment
%code requires {
// clang-format on

#include <cinttypes>
#include <cstdint>
#include <memory>

#include <LampCab/OutputStream.hpp>

#include "Parser.hpp"
#include "Photo_EvData.hpp"
#include "Photo_Scanner.hpp"

// clang-format off
}

// local declarations & some local definitions
%code {
// clang-format on

#include "Unit.hpp"

#define photo_yylex(scanner) (scanner)->lex<symbol_type>(location_type())

// clang-format off
}

// Bison Declarations --------------------------------------------------------------------------------------------------
// tokens
%define api.token.prefix {TOKEN_}
%token
    END 0                "end of file"
    '\n'                 "end of line"
    ' '                  "space"
    ';'                  "‘;’"
    APERTURE_PREFIX      "aperture prefix"
    SHUTTER_SPEED_PREFIX "shutter speed prefix"
    's'                  "second unit"
    ISO                  "‘ISO’"
    ASA                  "‘ASA’"
    DIN                  "‘DIN’"
    DEGREE_SIGN          "‘°’"
    'P'                  "Program AE"
    'S'                  "Shutter-priority AE"
    'A'                  "Aperture-priority AE"
    'M'                  "Manual exposure"
    '\a'                 "unknown token"
;

%token <std::int_fast32_t> INTEGER "integer"
%token <double>            DECIMAL "decimal number"
%token <double>            '#'     "very long number"

// nonterminal symbols
%type <std::int_fast32_t>    immediate.integer
%type <double>               immediate.decimal
%type <EvData>               expression
%type <EvData::Aperture>     immediate.aperture
%type <EvData::ShutterSpeed> immediate.shutter-speed
%type <EvData::Sensitivity>  immediate.sensitivity

%% // Grammar Rules ----------------------------------------------------------------------------------------------------
%start input;

input:
  %empty
| input command
;

command:
                            command.end
| space.optional expression command.end { std::printf("[Av=f/%f, Tv=1/%f or %f s, Sv=ISO %" PRIiFAST32 "/%.1f°, Bias=%+hi, Bv=%+f]\n", $[expression].Av.toFNumber().value_or(NAN), $[expression].Tv.toReverseSeconds().value_or(NAN), $[expression].Tv.toSeconds().value_or(NAN), $[expression].Sv.toIso().value_or(-1), $[expression].Sv.toDin().value_or(NAN), static_cast<std::int16_t>($[expression].bias), $[expression].Bv); }
| space.optional error      command.end { yyerrok; }
;

expression:
  immediate.aperture      { $$.Av = $1; }
| immediate.shutter-speed { $$.Tv = $1; }
| immediate.sensitivity   { $$.Sv = $1; }
;

immediate.aperture:
  APERTURE_PREFIX immediate.decimal { if (Unit::apertureFromFNumber($$, $[immediate.decimal])) {error(@$, "aperture out of bounds"); YYERROR;} }
;

immediate.shutter-speed:
  SHUTTER_SPEED_PREFIX immediate.integer                    { if (Unit::shutterSpeedFromReverseSeconds($$, $[immediate.integer])) {error(@$,    "speed out of bounds"); YYERROR;} }
|                      immediate.integer space.optional 's' { if (Unit::shutterSpeedFromSeconds       ($$, $[immediate.integer])) {error(@$, "duration out of bounds"); YYERROR;} }
;

immediate.sensitivity:
  ISO space.optional immediate.integer                    { if (Unit::sensitivityFromIso($$, $[immediate.integer])) {error(@$, "sensitivity out of bounds"); YYERROR;} }
| ASA space.optional immediate.integer                    { if (Unit::sensitivityFromIso($$, $[immediate.integer])) {error(@$, "sensitivity out of bounds"); YYERROR;} }
|                    immediate.integer space.optional ISO { if (Unit::sensitivityFromIso($$, $[immediate.integer])) {error(@$, "sensitivity out of bounds"); YYERROR;} }
|                    immediate.integer space.optional ASA { if (Unit::sensitivityFromIso($$, $[immediate.integer])) {error(@$, "sensitivity out of bounds"); YYERROR;} }
| ISO space.optional immediate.integer DEGREE_SIGN        { if (Unit::sensitivityFromDin($$, $[immediate.integer])) {error(@$, "sensitivity out of bounds"); YYERROR;} }
| DIN space.optional immediate.integer                    { if (Unit::sensitivityFromDin($$, $[immediate.integer])) {error(@$, "sensitivity out of bounds"); YYERROR;} }
|                    immediate.integer space.optional DIN { if (Unit::sensitivityFromDin($$, $[immediate.integer])) {error(@$, "sensitivity out of bounds"); YYERROR;} }
;

immediate.decimal:
  DECIMAL
| immediate.integer { $$ = static_cast<double>($1); }
;

immediate.integer: INTEGER;

command.end:
  space.optional ';'
| space.optional '\n'
;

space.optional: %empty | ' ';
