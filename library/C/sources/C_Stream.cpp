/*
 * Copyright © 2018-2020 Thierry Simeao
 *
 * This file is part of Lampadaire cabalistique.
 *
 * Lampadaire cabalistique is free software: you can redistribute it and/or modify it under the terms of the GNU Affero
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * Lampadaire cabalistique is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License
 * for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with Lampadaire cabalistique.
 * If not, see <https://www.gnu.org/licenses/>.
 */
#include "C_Stream.hpp"

#if __has_include(<unistd.h>)
#include <unistd.h>
#define IS_INTERACTIVE(stdFile) isatty(fileno(stdFile))
#else
#define IS_INTERACTIVE(stdFile) nullptr
#endif

C::Stream::Stream(std::FILE *file) : C::Stream::Stream(file, IS_INTERACTIVE(file)) {}

std::streamsize C::Stream::read(char *buffer, std::streamsize lengthMax) {
    std::streamsize length = streamBuffer.sgetn(buffer, lengthMax);
    return !streamBuffer ? -1 : length;
}

C::Stream::operator std::ostream &() { return outputStream; }
C::Stream::operator bool() const { return static_cast<bool>(streamBuffer); }
bool C::Stream::operator!() const { return !streamBuffer; }

C::Stream::Buffer::~Buffer() { std::fflush(streamC); }

std::streamsize C::Stream::Buffer::xsgetn(char *buffer, std::streamsize lengthMax) {
    if (streamLampCab.isInteractive) {
        std::streamsize length = 0;
        while (length < lengthMax) {
            int character = std::fgetc(streamC);
            switch (character) {
            case '\n':
                buffer[length++] = '\n';
                [[fallthrough]];
            case EOF:
                return length;

            default:
                buffer[length++] = character;
            }
        }
        return length;
    }

    return std::fread(buffer, 1, static_cast<std::size_t>(lengthMax), streamC);
}

std::streamsize C::Stream::Buffer::xsputn(const char *buffer, std::streamsize length) {
    return std::fwrite(buffer, 1, static_cast<std::size_t>(length), streamC);
}

int C::Stream::Buffer::sync() { return 0 == std::fflush(streamC) ? 0 : -1; }
