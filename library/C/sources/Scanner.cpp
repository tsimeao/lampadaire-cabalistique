/*
 * Copyright © 2018-2019 Thierry Simeao
 *
 * This file is part of Lampadaire cabalistique.
 *
 * Lampadaire cabalistique is free software: you can redistribute it and/or modify it under the terms of the GNU Affero
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * Lampadaire cabalistique is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License
 * for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with Lampadaire cabalistique.
 * If not, see <https://www.gnu.org/licenses/>.
 */
#include "Scanner.hpp"

#include <cctype>
#include <clocale>
#include <stdexcept>
#include <string>
#include <utility>

Scanner::Scanner(std::unique_ptr<LampCab::InputStream> &&      inputStream,
                 const std::shared_ptr<LampCab::OutputStream> &outputStream) noexcept
    : in(std::move(inputStream)), out(outputStream), positionInFile(0), tabStop(8) {}

void Scanner::updateLocation(Parser::Location &location, const char *byte_ptr /* text */, int length) noexcept {
    // store the location of the first byte
    location.begin.line             = positionInFile.line;
    location.begin.column.character = positionInFile.column.character + 1;
    location.begin.column.position  = positionInFile.column.position + 1;

    // update the file position
    do {
        const char byte = *(--length, byte_ptr++);

        if (0b1011'1111 == static_cast<std::uint8_t>(byte | 0b0011'1111)) {
            continue; // 10xx xxxx → trailing byte of a multi-byte character
        }

        ++positionInFile.column.character;
        switch (byte) {
        default:
            ++positionInFile.column.position;
            break;
        case '\t':
            positionInFile.column.position += tabStop;
            positionInFile.column.position -= positionInFile.column.position % tabStop;
            break;
        case '\v': /* uncounted */;
        }
    } while (length > 0);

    // store the location of the last byte
    location.end = positionInFile;
}

void Scanner::updateLocationForEndOfLine(Parser::Location &location) noexcept {
    // indicate the last character instead of the “end of line” character(s)
    location.end.column.character = --location.begin.column.character;
    location.end.column.position  = --location.begin.column.position;

    ++positionInFile.line;
    positionInFile.column = {0, 0};
}

std::int_fast32_t Scanner::readInteger(const char *byte_ptr /* text */, int length, int multiplier) {
    std::int_fast32_t integer        = 0;
    bool              overflow       = false;
    bool              separatorFound = false;

    do {
        const char byte = *(--length, byte_ptr++);

        if (std::isdigit(byte)) {
            overflow |= __builtin_mul_overflow(integer, 10, &integer);
            overflow |= __builtin_add_overflow(integer, byte - '0', &integer);

            if (separatorFound) {
                multiplier /= 10;
                if (multiplier < 10)
                    break;
            }
        } else if (byte == '.' || byte == ',') {
            if (separatorFound)
                break;
            separatorFound = true;
        }
    } while (length > 0);

    if (multiplier > 1) {
        overflow |= __builtin_mul_overflow(integer, multiplier, &integer);
    }

    if (overflow) {
        throw std::out_of_range("Scanner::readInteger: out of range");
    }

    return integer;
}

double Scanner::readDecimal(const char *byte_ptr /* text */, int length) {
    std::string numberAsString;

    // convert the text to the locale format
    do {
        const char byte = *(--length, byte_ptr++);

        if (std::isdigit(byte)) {
            numberAsString.push_back(byte);
        } else if (byte == '.' || byte == ',') {
            numberAsString.push_back(std::localeconv()->decimal_point[0]);
        }
    } while (length > 0);

    return std::stod(numberAsString);
}
