/*
 * Copyright © 2018-2020 Thierry Simeao
 *
 * This file is part of Lampadaire cabalistique.
 *
 * Lampadaire cabalistique is free software: you can redistribute it and/or modify it under the terms of the GNU Affero
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * Lampadaire cabalistique is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License
 * for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with Lampadaire cabalistique.
 * If not, see <https://www.gnu.org/licenses/>.
 */
#include <lamp_cab.h>

const char *const lamp_cab_version = "@PROJECT_VERSION@"
#ifndef NDEBUG
                                     " [@CPU_ARCHITECTURE@]"
#endif
    ;

const char *const lamp_cab_revision      = "@GIT_REVISION@";
const char *const lamp_cab_revision_date = "@GIT_REVISION_DATE@";
const char *const flex_version           = "@FLEX_VERSION@";
const char *const bison_version          = "@BISON_VERSION@";
