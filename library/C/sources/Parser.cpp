/*
 * Copyright © 2018-2019 Thierry Simeao
 *
 * This file is part of Lampadaire cabalistique.
 *
 * Lampadaire cabalistique is free software: you can redistribute it and/or modify it under the terms of the GNU Affero
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * Lampadaire cabalistique is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License
 * for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with Lampadaire cabalistique.
 * If not, see <https://www.gnu.org/licenses/>.
 */
#include "Parser.hpp"

#include <memory>

#include <lamp_cab.h>

#include "C_Stream.hpp"
#include "Photo_Parser.hpp"

#define COLOR_GREEN "\033[1;32m"
#define COLOR_CLEAR "\033[0m"

namespace Parser {

void printError(std::ostream &output, const Location &location, const std::string &message, bool interactive,
                bool color) {
    if (interactive) {
        for (int i = 1; i < location.begin.column.position; ++i) {
            output << ' ';
        }
        if (location.begin.line == location.end.line) {
            if (location.begin.column.position == location.end.column.position) {
                output << (color ? COLOR_GREEN "↑" COLOR_CLEAR "\n" : "↑\n");
            } else {
                output << (color ? COLOR_GREEN "❮" : "❮");
                for (int i = location.begin.column.position + 1; i < location.end.column.position; ++i) {
                    output << '-';
                }
                output << (color ? "❯" COLOR_CLEAR "\n" : "❯\n");
            }
        } else { // several lines
            int i = location.begin.column.position - 1;
            while (i > 0) {
                output << ' ';
                --i;
            }
            output << (color ? COLOR_GREEN "↿...\n" : "↿...\n");
            i = location.end.column.position - 1;
            while (i > 3) {
                output << ' ';
                --i;
            }
            while (i > 0) {
                output << '.';
                --i;
            }
            output << (color ? "↾" COLOR_CLEAR "\n" : "↾\n");
        }
    }

    output << location << ": " << message << '\n';
}

} // namespace Parser

std::ostream &operator<<(std::ostream &output, const Parser::Location &location) {
    output << location.begin.line << '.' << location.begin.column.position;

    if (location.begin.line != location.end.line) {
        output << '-' << location.end.line << '.' << location.end.column.position;
    } else if (location.begin.column.position != location.end.column.position) {
        output << '-' << location.end.column.position;
    }

    return output;
}

// Photo_Parser.y ------------------------------------------------------------------------------------------------------
void Photo::Parser::error(const location_type &location, const std::string &message) {
    ::Parser::printError(*errorStream, location, message, scanner->getIn().isInteractive, errorStream->isColorCapable);
}

extern "C" int photo_parse(FILE *input_stream, FILE *output_stream, FILE *error_stream) {
    std::shared_ptr<Photo::Scanner> scanner(              //
        std::make_shared<Photo::Scanner>(                 //
            std::make_unique<C::Stream>(input_stream),    //
            std::make_shared<C::Stream>(output_stream))); //

    std::unique_ptr<Photo::Parser> parser(               //
        std::make_unique<Photo::Parser>(                 //
            scanner,                                     //
            std::make_shared<C::Stream>(error_stream))); //

    return parser->parse();
}
