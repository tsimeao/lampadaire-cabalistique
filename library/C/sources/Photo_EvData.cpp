/*
 * Copyright © 2019-2020 Thierry Simeao
 *
 * This file is part of Lampadaire cabalistique.
 *
 * Lampadaire cabalistique is free software: you can redistribute it and/or modify it under the terms of the GNU Affero
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * Lampadaire cabalistique is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License
 * for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with Lampadaire cabalistique.
 * If not, see <https://www.gnu.org/licenses/>.
 */
#include "Photo_EvData.hpp"

#include <cstdlib>
#include <ios>
#include <sstream>
#include <streambuf>
#include <string_view>

#include "Unit.hpp"
#include "Utils.hpp"

namespace {

constexpr double TWO_RAISED_TO_THE_POWER_OF_N_DOT_5[19] = {
    0.0028, // ≈ √2 × 2⁻⁹
    0.0056, // ≈ √2 × 2⁻⁸
    0.011,  // ≈ √2 × 2⁻⁷
    0.022,  // ≈ √2 × 2⁻⁶
    0.045,  // ≈ √2 × 2⁻⁵
    0.09,   // ≈ √2 × 2⁻⁴
    0.18,   // ≈ √2 × 2⁻³
    0.35,   // ≈ √2 × 2⁻²
    0.7,    // ≈ √2 × 2⁻¹
    1.4,    // ≈ √2
    2.8,    // ≈ √2 × 2¹
    5.6,    // ≈ √2 × 2²
    11,     // ≈ √2 × 2³
    22,     // ≈ √2 × 2⁴
    45,     // ≈ √2 × 2⁵
    90,     // ≈ √2 × 2⁶
    180,    // ≈ √2 × 2⁷
    350,    // ≈ √2 × 2⁸
    700,    // ≈ √2 × 2⁹
};

std::optional<double> toSpeed(Photo::EvData::ShutterSpeed speed) {
    if (!speed) {
        return std::nullopt;
    }

    const Photo::EvData::ShutterSpeed::IntegerType value = speed;

    if (value & 1) { // ¬⅓-stop
        switch (std::div_t apexStop = std::div(value, 6); apexStop.rem) {
        // ½-stop
        case -3:
            --apexStop.quot;
            [[fallthrough]];
        case +3: {
            const std::div_t logSpeed = std::div(apexStop.quot, 10);
            return (TWO_RAISED_TO_THE_POWER_OF_N_DOT_5 + 9)[logSpeed.rem] * std::pow(1'000.0, logSpeed.quot);
        }
        }
    } else { // ⅓-stop
        static constexpr std::int_least8_t THIRD_STOP_PAIR_OF_MULTIPLIERS[19][2] = {
            {0, -3},  // 10⁰  × 2⁻³ = 0.125
            {+1, -6}, // 10⁺¹ × 2⁻⁶ = 0.15625
            {-1, +1}, // 10⁻¹ × 2⁺¹ = 0.2
            {0, -2},  // 10⁰  × 2⁻² = 0.25
            {+1, -5}, // 10⁺¹ × 2⁻⁵ = 0.3125
            {-1, +2}, // 10⁻¹ × 2⁺² = 0.4
            {0, -1},  // 10⁰  × 2⁻¹ = 0.5
            {+1, -4}, // 10⁺¹ × 2⁻⁴ = 0.625
            {-1, +3}, // 10⁻¹ × 2⁺³ = 0.8
            {0, 0},   // 10⁰  × 2⁰  = 1
            {+1, -3}, // 10⁺¹ × 2⁻³ = 1.25
            {-1, +4}, // 10⁻¹ × 2⁺⁴ = 1.6
            {0, +1},  // 10⁰  × 2⁺¹ = 2
            {+1, -2}, // 10⁺¹ × 2⁻² = 2.5
            {-1, +5}, // 10⁻¹ × 2⁺⁵ = 3.2
            {0, +2},  // 10⁰  × 2⁺² = 4
            {+1, -1}, // 10⁺¹ × 2⁻¹ = 5
            {-1, +6}, // 10⁻¹ × 2⁺⁶ = 6.4
            {0, +3},  // 10⁰  × 2⁺³ = 8
        };

        const std::div_t logSpeed = std::div(value >> 1, 10);

        if (Utils::isInRange<0, 38>(value)) { // speed ∈ [1, 80]
            static constexpr double STANDARD_LOW_VALUE[10][2] = {
                {1, 10},   // 10^0.0, 10^1.0
                {1.3, 13}, // 10^0.1, 10^1.1
                {1.6, 15}, // 10^0.2, 10^1.2
                {2, 20},   // 10^0.3, 10^1.3
                {2.5, 25}, // 10^0.4, 10^1.4
                {3, 30},   // 10^0.5, 10^1.5
                {4, 40},   // 10^0.6, 10^1.6
                {5, 50},   // 10^0.7, 10^1.7
                {6, 60},   // 10^0.8, 10^1.8
                {8, 80},   // 10^0.9, 10^1.9
            };

            return STANDARD_LOW_VALUE[logSpeed.rem][logSpeed.quot];
        }

        return std::ldexp(
            std::pow(10.0, static_cast<double>(logSpeed.quot + (THIRD_STOP_PAIR_OF_MULTIPLIERS + 9)[logSpeed.rem][0])),
            (THIRD_STOP_PAIR_OF_MULTIPLIERS + 9)[logSpeed.rem][1]);
    }

    return std::pow(10.0, value / (6.0 * 10 / 3));
}

} // namespace

std::optional<double> Photo::EvData::Aperture::toFNumber() const {
    if (!*this) {
        return std::nullopt;
    }

    double     fNumber;
    std::div_t apexDoubleStop = std::div(value, 6 * 2);

    if (apexDoubleStop.rem < 0) {
        apexDoubleStop.rem += (6 * 2);
        --apexDoubleStop.quot;
    }

    if (apexDoubleStop.rem & 1) { // ¬⅓-stop
        switch (apexDoubleStop.rem) {
        // ½-stop
        case 3:
            switch (apexDoubleStop.quot) {
            default:
                fNumber = std::ldexp(static_cast<double>(Unit::PRINCIPAL_4TH_ROOT_OF_2), apexDoubleStop.quot);
                break;
            case -1:
                return 0.6;
            }
            break;
        case 9:
            switch (apexDoubleStop.quot) {
            default:
                fNumber = std::ldexp(static_cast<double>(Unit::TWO_RAISED_TO_THE_POWER_OF_3_4TH), apexDoubleStop.quot);
                break;
            case -1:
                return 0.85;
            case 1:
                return 3.3;
            case 3:
                return 13.5;
            }
            break;

        default:
            switch (value) {
            case -1:
                return 0.95;
            case 11:
                return 1.88;
            case 29:
                return 5.3;

            default:
                return std::exp2(value / (6.0 * 2));
            }
        }
    } else { // ⅓-stop
        static constexpr double TWO_RAISED_TO_THE_POWER_OF_N_6TH[5] = {
            static_cast<double>(Unit::PRINCIPAL_6TH_ROOT_OF_2),          // 2^(1/6)
            static_cast<double>(Unit::PRINCIPAL_CUBE_ROOT_OF_2),         // 2^(2/6)
            static_cast<double>(Unit::PRINCIPAL_SQUARE_ROOT_OF_2),       // 2^(3/6)
            static_cast<double>(Unit::TWO_RAISED_TO_THE_POWER_OF_2_3RD), // 2^(4/6)
            static_cast<double>(Unit::TWO_RAISED_TO_THE_POWER_OF_5_6TH), // 2^(5/6)
        };

        switch (apexDoubleStop.rem) {
        // full stop
        case 0:
            return std::ldexp(1.0, apexDoubleStop.quot);
        case 6:
            if (Utils::isInRange<-4, +6>(apexDoubleStop.quot)) { // aperture ∈ [f/0.09, f/90]
                return (TWO_RAISED_TO_THE_POWER_OF_N_DOT_5 + 9)[apexDoubleStop.quot];
            }
            break;

        default:
            switch (value) {
            case 4:
                return 1.25;
            case 22:
                return 3.5;
            }
        }

        fNumber = std::ldexp((TWO_RAISED_TO_THE_POWER_OF_N_6TH - 1)[apexDoubleStop.rem >> 1], apexDoubleStop.quot);
    }

    if (fNumber <= 0.75) {
        if (fNumber <= 0.125) {
            fNumber = std::roundf(fNumber * 1'000.0) / 1'000.0;
        } else {
            fNumber = std::roundf(fNumber * 100.0) / 100.0;
        }
    } else {
        if (fNumber < 10.0) {
            fNumber = std::roundf(fNumber * 10.0) / 10.0;
        } else {
            fNumber = std::roundf(fNumber);
        }
    }

    return fNumber;
}

std::optional<double> Photo::EvData::ShutterSpeed::toSeconds() const { return ::toSpeed(ShutterSpeed(-value)); }
std::optional<double> Photo::EvData::ShutterSpeed::toReverseSeconds() const { return ::toSpeed(*this); }

std::optional<std::int_fast32_t> Photo::EvData::Sensitivity::toIso() const {
    if (!*this) {
        return std::nullopt;
    }

    std::int_fast32_t iso;
    std::div_t        apexStop = std::div(value, 6);

    if (apexStop.rem < 0) {
        apexStop.rem += 6;
        --apexStop.quot;
    }

    switch (apexStop.rem) {
    case 0: // full stop → origin = 100 ISO
        switch (apexStop.quot) {
        case -1:
            return 2;

        default:
            iso = 100;
            apexStop.quot -= 5; // 100 ISO ≘ +5
        }
        break;

    case 2: // stop + ⅓
        if (apexStop.quot >= 5) {
            // value ≥ 125 ISO → origin = 1,000 ISO
            iso = 1'000;
            apexStop.quot -= 8; // 1,000 ISO ≘ +8⅓
        } else {
            // value ≤ 64 ISO → origin = 1 ISO
            iso = 1;
            apexStop.quot -= -2; // 1 ISO ≘ −1⅔
        }
        break;

    case 4: // stop + ⅔
        if (apexStop.quot >= 8) {
            // value ≥ 1,250 ISO → origin = 10,000 ISO
            iso = 10'000;
            apexStop.quot -= 11; // 10,000 ISO ≘ +11⅔
        } else {
            // value ≤ 640 ISO → origin = 10 ISO
            iso = 10;
            apexStop.quot -= 1; // 10 ISO ≘ +1⅔
        }
        break;

    case 3: // stop + ½
        if (const double fIso =
                std::ldexp(static_cast<double>(Unit::PRINCIPAL_SQUARE_ROOT_OF_2 * 3.125), apexStop.quot);
            fIso >= 1'000) {
            iso = static_cast<std::int_fast32_t>(fIso / 100.0) * 100;
        } else if (fIso >= 100) {
            iso = static_cast<std::int_fast16_t>(fIso / 10.0) * 10;
        } else if (fIso >= 10) {
            iso = static_cast<std::int_fast32_t>(fIso);
        } else {
            iso = std::lround(fIso);
        }
        return iso;

    default:
        return std::lround(std::exp2(value / 6.0) * 3.125);
    }

    if (apexStop.quot < 0) {
        iso >>= -apexStop.quot;
    } else if (apexStop.quot > 0) {
        iso <<= apexStop.quot;
    }

    return iso;
}

std::optional<float> Photo::EvData::Sensitivity::toDin() const {
    return *this ? std::optional<float>(std::ldexp(static_cast<float>(value + 12), -1)) : std::nullopt;
}

Photo::EvData::Aperture::operator std::wstring() const {
    if (const std::optional<double> fNumber = toFNumber(); fNumber.has_value()) {
        std::wostringstream output;
        output.imbue(Unit::locale());
        output.precision(10);

        output << L"f/" << *fNumber;
        return output.str();
    }

    return std::wstring();
}

Photo::EvData::ShutterSpeed::operator std::wstring() const {
    if (!*this) {
        return std::wstring();
    }

    std::wostringstream output;
    output.imbue(Unit::locale());
    output.precision(10);

    if (value > 0) {
        output << L"1/" << *toReverseSeconds();
    } else {
        output << *toSeconds() << L" s";
    }
    return output.str();
}

Photo::EvData::Sensitivity::operator std::wstring() const {
    if (const std::optional<std::int_fast32_t> iso = toIso(); iso.has_value()) {
        std::wostringstream output;
        output.imbue(Unit::locale());

        output << L"ISO " << *iso;
        if (*iso <= 6) {
            switch (value) {
            case 4: // 5 ISO (⅓-stop)
            case 0: // 3 ISO
            case 6: // 6 ISO
                break;

            default:
                output << L'/' << *toDin() << L'°';
            }
        }
        return output.str();
    }

    return std::wstring();
}

std::wostream &Photo::operator<<(std::wostream &output, const Photo::EvData::Sensitivity sensitivity) {
    if (const std::wostream::sentry ok(output); ok) {
        std::wstreambuf &buffer        = *output.rdbuf();
        wchar_t          fillCharacter = output.fill();

        if (const std::wstring string = sensitivity; string.empty()) {
            for (std::streamsize i = output.width(); i > 0; --i) {
                buffer.sputc(fillCharacter);
            }
        } else if (std::streamsize nbMissingCharacters = output.width() - string.length(); nbMissingCharacters <= 0) {
            buffer.sputn(string.data(), string.size());
        } else {
            std::wstring_view substring = string;

            switch (output.flags() & std::wostream::adjustfield) {
            case std::wostream::internal:
                buffer.sputn(substring.data(), 4);
                substring.remove_prefix(4);
                fillCharacter = L' ';
                [[fallthrough]];
            case std::wostream::right:
                do {
                    buffer.sputc(fillCharacter);
                } while (--nbMissingCharacters > 0);
                buffer.sputn(substring.data(), substring.size());
                break;

            default: // left
                buffer.sputn(substring.data(), substring.size());
                do {
                    buffer.sputc(fillCharacter);
                } while (--nbMissingCharacters > 0);
            }
        }
        output.width(0);
    }
    return output;
}
