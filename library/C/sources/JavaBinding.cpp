/*
 * Copyright © 2018-2020 Thierry Simeao
 *
 * This file is part of Lampadaire cabalistique.
 *
 * Lampadaire cabalistique is free software: you can redistribute it and/or modify it under the terms of the GNU Affero
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * Lampadaire cabalistique is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License
 * for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with Lampadaire cabalistique.
 * If not, see <https://www.gnu.org/licenses/>.
 */
#include "io_gitlab_tsimeao_lampadaireCabalistique_LampCab.h"

#include <jni.h>

#include <lamp_cab.h>

jint JNI_OnLoad(JavaVM *, void *) { return JNI_VERSION_1_6; }
void JNI_OnUnload(JavaVM *, void *) {}

jstring Java_io_gitlab_tsimeao_lampadaireCabalistique_LampCab_getVersion(JNIEnv *env, jclass) {
    return env->NewStringUTF(lamp_cab_version);
}

jstring Java_io_gitlab_tsimeao_lampadaireCabalistique_LampCab_getRevision(JNIEnv *env, jclass) {
    return env->NewStringUTF(lamp_cab_revision);
}

jstring Java_io_gitlab_tsimeao_lampadaireCabalistique_LampCab_readRevisionDate(JNIEnv *env, jclass) {
    return env->NewStringUTF(lamp_cab_revision_date);
}

jstring Java_io_gitlab_tsimeao_lampadaireCabalistique_LampCab_getFlexVersion(JNIEnv *env, jclass) {
    return env->NewStringUTF(flex_version);
}

jstring Java_io_gitlab_tsimeao_lampadaireCabalistique_LampCab_getBisonVersion(JNIEnv *env, jclass) {
    return env->NewStringUTF(bison_version);
}
