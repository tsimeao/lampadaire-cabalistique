/*
 * Copyright © 2018-2020 Thierry Simeao
 *
 * This file is part of Lampadaire cabalistique.
 *
 * Lampadaire cabalistique is free software: you can redistribute it and/or modify it under the terms of the GNU Affero
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * Lampadaire cabalistique is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License
 * for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with Lampadaire cabalistique.
 * If not, see <https://www.gnu.org/licenses/>.
 */
#include "Unit.hpp"

#include <cerrno>
#include <cfenv>
#include <cmath>
#include <string>

#pragma STDC FENV_ACCESS ON

namespace {

class PunctuationFormat final : public std::numpunct<wchar_t> {
public:
    PunctuationFormat() : std::numpunct<wchar_t>::numpunct(1) {}

protected:
    wchar_t     do_thousands_sep() const override;
    std::string do_grouping() const override;
} punctuationFormat;

wchar_t PunctuationFormat::do_thousands_sep() const { return L'\''; }

std::string PunctuationFormat::do_grouping() const {
    static bool isRecursivelyCalled = false;

    if (isRecursivelyCalled) {
        using namespace std::literals::string_literals;
        return "\3"s;
    }

    try {
        isRecursivelyCalled  = true;
        std::string grouping = std::use_facet<std::numpunct<wchar_t>>(std::locale()).grouping();
        isRecursivelyCalled  = false;
        return grouping;
    } catch (...) {
        isRecursivelyCalled = false;
        throw;
    }
}

template <typename StopSixth, typename T>
bool linearToLogarithmic(StopSixth &logarithmic, T linear, std::int_fast8_t multiplier, bool roundToHalf,
                         T (*logFctPtr)(T)) {
    static constexpr std::int_fast8_t offsetToHalf[5] = {-1, +1, 0, -1, +1};

    // clear previous errors
    errno = 0;
    if (math_errhandling & MATH_ERREXCEPT) {
        std::feclearexcept(FE_ALL_EXCEPT);
    }

    const T        fLogValue = multiplier * logFctPtr(linear);
    const long int iLogValue = std::lround(fLogValue);

    if (math_errhandling & MATH_ERREXCEPT) {
        if (std::fetestexcept(FE_DIVBYZERO | FE_INVALID)) {
            logarithmic = StopSixth(std::signbit(fLogValue) ? StopSixth::MINIMUM : StopSixth::MAXIMUM);
            return true;
        }
    } else if (math_errhandling & MATH_ERRNO) {
        if (0 != errno) {
            logarithmic = StopSixth(std::signbit(fLogValue) ? StopSixth::MINIMUM : StopSixth::MAXIMUM);
            return true;
        }
    }

    if (iLogValue > StopSixth::MAXIMUM) {
        logarithmic = StopSixth(StopSixth::MAXIMUM);
        return true;
    }

    if (iLogValue < StopSixth::MINIMUM) {
        logarithmic = StopSixth(StopSixth::MINIMUM);
        return true;
    }

    logarithmic = StopSixth(roundToHalf ? iLogValue + (offsetToHalf + 2)[iLogValue % 3] : iLogValue);
    return false;
}

} // namespace

namespace Unit {

std::locale locale() { return std::locale(std::locale::classic(), &punctuationFormat); }

bool apertureFromFNumber(Photo::EvData::Aperture &Av, float fNumber, bool roundToHalf) {
    return linearToLogarithmic(Av, fNumber, 6 * 2, roundToHalf, &log2f);
}

bool shutterSpeedFromSeconds(Photo::EvData::ShutterSpeed &Tv, float time, bool roundToHalf) {
    return linearToLogarithmic(Tv, time, 6 * -10 / 3, roundToHalf, &log10f);
}

bool shutterSpeedFromReverseSeconds(Photo::EvData::ShutterSpeed &Tv, float speed, bool roundToHalf) {
    return linearToLogarithmic(Tv, speed, 6 * 10 / 3, roundToHalf, &log10f);
}

bool sensitivityFromIso(Photo::EvData::Sensitivity &Sv, std::int_fast32_t iso, bool roundToHalf) {
    return linearToLogarithmic(Sv, iso / 3.125f, 6, roundToHalf, &log2f);
}

bool sensitivityFromDin(Photo::EvData::Sensitivity &Sv, std::int_fast32_t degrees) {
    if (degrees > Photo::EvData::Sensitivity::MAXIMUM / 2 + 6) {
        Sv = Photo::EvData::Sensitivity(Photo::EvData::Sensitivity::MAXIMUM);
        return true;
    }

    if (degrees < Photo::EvData::Sensitivity::MINIMUM / 2 + 6) {
        Sv = Photo::EvData::Sensitivity(Photo::EvData::Sensitivity::MINIMUM);
        return true;
    }

    Sv = Photo::EvData::Sensitivity((degrees << 1) - 12); // × 2 − 12
    return false;
}

} // namespace Unit
