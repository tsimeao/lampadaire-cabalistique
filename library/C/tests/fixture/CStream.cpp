/*
 * Copyright ©2019 Thierry Simeao
 *
 * This file is part of Lampadaire cabalistique.
 *
 * Lampadaire cabalistique is free software: you can redistribute it and/or modify it under the terms of the GNU Affero
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * Lampadaire cabalistique is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License
 * for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with Lampadaire cabalistique.
 * If not, see <https://www.gnu.org/licenses/>.
 */
#include "CStream.hpp"

const char *const Test::CStream::FILE_CONTENT = "Line 1\n"   // UNIX®
                                                "Line 2\r"   // Apple® classic Mac OS®
                                                "Line 3\r\n" // Microsoft® Windows®
                                                "Line 4";    // no end of line

Test::CStream::~CStream() {
    if (nullptr != file) {
        std::fclose(file);
    }
}

void Test::CStream::SetUp() {
    ASSERT_NE((file = std::tmpfile()), nullptr) << "Set-up: unable to create a temporary test file";
    ASSERT_NE(std::fputs(FILE_CONTENT, file), EOF) << "Set-up: unable to fill the temporary test file";
    std::rewind(file);
}
