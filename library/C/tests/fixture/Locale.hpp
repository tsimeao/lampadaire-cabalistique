/*
 * Copyright ©2020 Thierry Simeao
 *
 * This file is part of Lampadaire cabalistique.
 *
 * Lampadaire cabalistique is free software: you can redistribute it and/or modify it under the terms of the GNU Affero
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * Lampadaire cabalistique is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License
 * for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with Lampadaire cabalistique.
 * If not, see <https://www.gnu.org/licenses/>.
 */
#pragma once
#include <gtest/gtest.h>

#include <locale>
#include <string>

namespace Test {

class Locale : public ::testing::Test {
private:
    class NumericPunctuationFacet final : public std::numpunct<wchar_t> {
    public:
        NumericPunctuationFacet() : std::numpunct<wchar_t>::numpunct(1) {}

    protected:
        wchar_t     do_decimal_point() const override;
        wchar_t     do_thousands_sep() const override;
        std::string do_grouping() const override;
    } numericPunctuationFacet;

protected:
    const std::locale ORIGINAL_LOCALE;

    Locale();
    ~Locale() override;
    void TearDown() override;
};

} // namespace Test
