/*
 * Copyright ©2020 Thierry Simeao
 *
 * This file is part of Lampadaire cabalistique.
 *
 * Lampadaire cabalistique is free software: you can redistribute it and/or modify it under the terms of the GNU Affero
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * Lampadaire cabalistique is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License
 * for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with Lampadaire cabalistique.
 * If not, see <https://www.gnu.org/licenses/>.
 */
#pragma once
#include <gtest/gtest.h>

#include <bitset>
#include <cmath>
#include <cstddef>
#include <locale>
#include <regex>
#include <sstream>
#include <string>
#include <tuple>
#include <type_traits>
#include <utility>

#include "Locale.hpp"

namespace Test {

class ToString : public Locale {
protected:
    ToString() = default;

private:
    std::wregex         regularExpression;
    std::wistringstream numberExtractor;

protected:
    // configure
    void setExtractionLocale(const std::locale &locale) {
        regularExpression.imbue(locale);
        numberExtractor.imbue(locale);
    }
    void setRegularExpression(const wchar_t *regex) {
        regularExpression.assign(regex, std::wregex::ECMAScript | std::wregex::collate | std::wregex::optimize);
    }

    // check
    template <typename... Types>
    std::tuple<std::bitset<sizeof...(Types)>, Types...> match(const std::wstring &text, const wchar_t *representing);

private:
    template <typename T> static constexpr T getNullValue() noexcept {
        static_assert(std::is_scalar_v<T> && !std::is_const_v<T>);

        if constexpr (std::is_pointer_v<T>) {
            return nullptr;
        } else {
            static_assert(std::is_arithmetic_v<T>);

            if constexpr (std::is_same_v<std::remove_volatile_t<T>, bool>) {
                return false;
            } else if constexpr (std::is_floating_point_v<T>) {
                return NAN;
            } else { // integral
                return 0;
            }
        }
    }

    template <typename... Type, std::size_t... Index>
    static void initialize(std::tuple<std::bitset<sizeof...(Type)>, Type...> &tuple,
                           const std::index_sequence<Index...>) {
        static_assert(sizeof...(Type) == sizeof...(Index));
        (std::get<0>(tuple).reset(), ..., (std::get<Index + 1>(tuple) = getNullValue<Type>()));
    }

    template <typename... Type, std::size_t... Index>
    void extractNumbers(std::tuple<std::bitset<sizeof...(Type)>, Type...> &tuple, const std::wsmatch &matches,
                        const std::index_sequence<Index...>) {
        static_assert(sizeof...(Type) == sizeof...(Index));
        (((std::get<0>(tuple)[Index] = matches.size() > Index + 1 && matches.length(Index + 1) > 0)
              ? (numberExtractor.str(matches.str(Index + 1)),   //
                 numberExtractor >> std::get<Index + 1>(tuple), //
                 numberExtractor.clear())                       //
              : (std::get<Index + 1>(tuple) = getNullValue<Type>(), void())),
         ...);
    }
};

template <typename... Types>
std::tuple<std::bitset<sizeof...(Types)>, Types...> ToString::match(const std::wstring &text,
                                                                    const wchar_t *     representing) {
    namespace flag = std::regex_constants;

    std::wsmatch                                        matches;
    std::tuple<std::bitset<sizeof...(Types)>, Types...> result;

    std::regex_match(text, matches, regularExpression,
                     flag::match_not_bow | flag::match_not_eow | flag::match_any | flag::match_not_null);

    if (matches.empty()) {
        ADD_FAILURE() << "Problem: ‘" << text << "’ doesn't look like " << representing << '.';
        initialize(result, std::index_sequence_for<Types...>());
    } else {
        extractNumbers(result, matches, std::index_sequence_for<Types...>());
    }

    return result;
}

} // namespace Test
