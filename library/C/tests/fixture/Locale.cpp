/*
 * Copyright ©2020 Thierry Simeao
 *
 * This file is part of Lampadaire cabalistique.
 *
 * Lampadaire cabalistique is free software: you can redistribute it and/or modify it under the terms of the GNU Affero
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * Lampadaire cabalistique is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License
 * for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with Lampadaire cabalistique.
 * If not, see <https://www.gnu.org/licenses/>.
 */
#include "Locale.hpp"

Test::Locale::Locale() : ORIGINAL_LOCALE(std::locale::classic(), &numericPunctuationFacet) {
    std::locale::global(ORIGINAL_LOCALE);
}

Test::Locale::~Locale() {
    try {
        std::locale::global(std::locale::classic());
    } catch (...) {
    }
}

void Test::Locale::TearDown() {
    EXPECT_EQ(std::locale(), ORIGINAL_LOCALE) << "Tear-down: the global locale has changed";
}

wchar_t Test::Locale::NumericPunctuationFacet::do_decimal_point() const { return L'*'; }
wchar_t Test::Locale::NumericPunctuationFacet::do_thousands_sep() const { return L'!'; }

std::string Test::Locale::NumericPunctuationFacet::do_grouping() const {
    using namespace std::literals::string_literals;
    return "\1\4"s;
}
