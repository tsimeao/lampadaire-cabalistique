/*
 * Copyright © 2019-2020 Thierry Simeao
 *
 * This file is part of Lampadaire cabalistique.
 *
 * Lampadaire cabalistique is free software: you can redistribute it and/or modify it under the terms of the GNU Affero
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * Lampadaire cabalistique is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License
 * for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with Lampadaire cabalistique.
 * If not, see <https://www.gnu.org/licenses/>.
 */
#include "Unit_EvEntry.hpp"

#include <cmath>
#include <list>
#include <sstream>
#include <string>

#include "fixture/ToStream.hpp"
#include "fixture/ToString.hpp"

#include "Unit.hpp"
#include "comparators.hpp"

using Aperture    = Photo::EvData::Aperture;
using IntegerType = Aperture::IntegerType;
using EvEntry     = Test::EvEntryPartialStop<Aperture, float>;

TEST(Input, fNumber) {
    const std::list<::Test::EvEntryFullStop<Aperture, float>> apertures_fullStop = {
        {-2, 0.5}, //
        {-1, 0.7}, //
        {0, 1},    //
        {1, 1.4},  //
        {2, 2},    //
        {3, 2.8},  //
        {4, 4},    //
        {5, 5.6},  //
        {6, 8},    //
        {7, 11},   //
        {8, 16},   //
        {9, 22},   //
        {10, 32},  //
        {11, 45},  //
        {12, 64},  //
        {13, 90},  //
        {14, 128}, //
        {15, 181}, //
        {16, 256}, //
    };
    const std::list<EvEntry> apertures_partialStop = {
        {-10, 0.56},                                                    // f/0.5 −⅓
        {-9, 0.6},                                                      //        ½
        {-8, 0.63},                                                     // f/0.7 +⅓
                                                                        // f/0.7
        {-4, 0.8},                                                      // f/0.7 −⅓
        {-3, 0.8, EvEntry::NON_STANDARD | EvEntry::NEED_HALF_ROUNDING}, //        ½
        {-3, 0.85, EvEntry::STANDARD},                                  //        ½
        {-2, 0.9},                                                      // f/1   +⅓
                                                                        // f/1
        {2, 1.1},                                                       // f/1   −⅓
        {3, 1.2},                                                       //        ½
        {4, 1.25},                                                      // f/1.4 +⅓ (f/1.3)
                                                                        // f/1.4
        {8, 1.6},                                                       // f/1.4 −⅓
        {9, 1.7},                                                       //        ½
        {10, 1.8},                                                      // f/2   +⅓
                                                                        // f/2
        {14, 2.2},                                                      // f/2   −⅓
        {15, 2.4},                                                      //        ½
        {16, 2.5},                                                      // f/2.8 +⅓
                                                                        // f/2.8
        {20, 3.2},                                                      // f/2.8 −⅓
        {21, 3.3, EvEntry::STANDARD},                                   //        ½
        {21, 3.4, EvEntry::NON_STANDARD},                               //        ½
        {22, 3.5, EvEntry::STANDARD},                                   // f/4   +⅓
        {22, 3.6, EvEntry::NON_STANDARD},                               // f/4   +⅓
                                                                        // f/4
        {26, 4.5},                                                      // f/4   −⅓
        {27, 4.8},                                                      //        ½
        {28, 5},                                                        // f/5.6 +⅓
                                                                        // f/5.6
        {32, 6.3},                                                      // f/5.6 −⅓
        {33, 6.7},                                                      //        ½
        {34, 7.1},                                                      // f/8   +⅓
                                                                        // f/8
        {38, 9},                                                        // f/8   −⅓
        {39, 9.5},                                                      //        ½
        {40, 10},                                                       // f/11  +⅓
                                                                        // f/11
        {44, 12.7, EvEntry::NON_STANDARD},                              // f/11  −⅓
        {44, 13, EvEntry::STANDARD},                                    // f/11  −⅓
        {45, 13, EvEntry::NON_STANDARD | EvEntry::NEED_HALF_ROUNDING},  //        ½
        {45, 13.5, EvEntry::STANDARD},                                  //        ½
        {46, 14},                                                       // f/16  +⅓
                                                                        // f/16
        {50, 18},                                                       // f/16  −⅓
        {51, 19},                                                       //        ½
        {52, 20},                                                       // f/22  +⅓
                                                                        // f/22
        {56, 25},                                                       // f/22  −⅓
        {57, 27},                                                       //        ½
        {58, 29},                                                       // f/32  +⅓
                                                                        // f/32
        {62, 36},                                                       // f/32  −⅓
        {63, 38},                                                       //        ½
        {64, 40},                                                       // f/45  +⅓
                                                                        // f/45
        {68, 51},                                                       // f/45  −⅓
        {69, 54},                                                       //        ½
        {70, 57},                                                       // f/64  +⅓
                                                                        // f/64
        {74, 72},                                                       // f/64  −⅓
        {75, 76},                                                       //        ½
        {76, 81},                                                       // f/90  +⅓
                                                                        // f/90
        {80, 102},                                                      // f/90  −⅓
        {81, 108},                                                      //        ½
        {82, 114},                                                      // f/128 +⅓
                                                                        // f/128
        {86, 144},                                                      // f/128 −⅓
        {87, 152},                                                      //        ½
        {88, 161},                                                      // f/181 +⅓
                                                                        // f/181
        {92, 203},                                                      // f/181 −⅓
        {93, 215},                                                      //        ½
        {94, 228},                                                      // f/256 +⅓

        // others
        {-1, 0.95, EvEntry::STANDARD},
        {11, 1.88, EvEntry::STANDARD},
        {29, 5.3, EvEntry::STANDARD},
        {Aperture::MINIMUM, 0.02, EvEntry::STANDARD},

        // bounds
        {Aperture::MINIMUM, std::exp2(static_cast<float>(Aperture::MINIMUM) / (6 * 2)), EvEntry::NON_STANDARD},
        {Aperture::MAXIMUM, std::exp2(static_cast<float>(Aperture::MAXIMUM) / (6 * 2)), EvEntry::NON_STANDARD},
    };

    for (auto aperture : apertures_fullStop) {
        aperture.test(&Unit::apertureFromFNumber, &Aperture::toFNumber, "f/", "");
    }

    for (auto aperture : apertures_partialStop) {
        aperture.test(&Unit::apertureFromFNumber, &Aperture::toFNumber, "f/", "");
    }

    // limits
    Aperture apexAperture;

    apexAperture = Aperture(0);
    EXPECT_TRUE(Unit::apertureFromFNumber(apexAperture, 0)) << "Problem: ‘f/0’ doesn't give an overflow.";
    EXPECT_EQ(static_cast<IntegerType>(apexAperture), Aperture::MINIMUM)
        << "Problem: ‘f/0’ has been converted to ‘" << static_cast<IntegerType>(apexAperture)
        << "’ instead of ‘Aperture::MINIMUM’.";

    apexAperture = Aperture(0);
    EXPECT_TRUE(Unit::apertureFromFNumber(apexAperture, INFINITY)) << "Problem: ‘f/∞’ doesn't give an overflow.";
    EXPECT_EQ(static_cast<IntegerType>(apexAperture), Aperture::MAXIMUM)
        << "Problem: ‘f/∞’ has been converted to ‘" << static_cast<IntegerType>(apexAperture)
        << "’ instead of ‘Aperture::MAXIMUM’.";
}

TEST(Output, fNumber) {
    std::optional<double> fNumber;
    double                expectedFNumber = NAN;

    for (IntegerType internal = Aperture::MINIMUM | 1; internal <= Aperture::MAXIMUM; internal += 2) {
        if (const IntegerType subStop = internal % 6; subStop == 3 || subStop == -3) {
            continue;
        }

        fNumber = Aperture(internal).toFNumber();
        switch (internal) {
        case -1:
            expectedFNumber = 0.95;
            break;
        case 11:
            expectedFNumber = 1.88;
            break;
        case 29:
            expectedFNumber = 5.3;
            break;
        default:
            expectedFNumber = std::exp2(internal / 12.0);
        }

        ASSERT_TRUE(fNumber.has_value()) << "Problem: ‘Aperture(" << internal << ")’ gives no output.";
        EXPECT_EQ(*fNumber, expectedFNumber) << std::setprecision(10) << "Problem: " << internal << " is displayed ‘f/"
                                             << *fNumber << "’ instead of ‘f/" << expectedFNumber << "’.";
    }
}

TEST(RoundTrip, fNumber) {
    struct {
        Aperture source, result;
    } internal;
    std::optional<double> fNumber;

    for (IntegerType i = Aperture::MINIMUM; i <= Aperture::MAXIMUM; ++i) {
        internal.source = Aperture(i);
        internal.result = Aperture(Aperture::UNDEFINED);

        fNumber = internal.source.toFNumber();
        ASSERT_TRUE(fNumber.has_value()) << "Problem: ‘Aperture(" << i << ")’ gives no output.";
        ASSERT_FALSE(Unit::apertureFromFNumber(internal.result, *fNumber))
            << "Problem: ‘f/" << *fNumber << "’ gives an overflow.";
        EXPECT_EQ(static_cast<IntegerType>(internal.source), static_cast<IntegerType>(internal.result))
            << "Problem: " << static_cast<IntegerType>(internal.source) << " → ‘f/" << *fNumber << "’ → "
            << static_cast<IntegerType>(internal.result);
    }
}

namespace Test {
TEST_F(ToString, Aperture) {
    Aperture     apexAperture;
    std::wstring textAperture;

    setExtractionLocale(Unit::locale());
    setRegularExpression(L"f/((?:[1-9]\\d*(?:'\\d+)*|0)(?:\\.\\d*[1-9])?)");

    for (IntegerType internal = Aperture::MINIMUM; internal <= Aperture::MAXIMUM; ++internal) {
        apexAperture                                = Aperture(internal);
        textAperture                                = static_cast<std::wstring>(apexAperture);
        std::tuple<std::bitset<1>, float> readValue = match<float>(textAperture, L"an aperture");

        if (std::get<0>(readValue)[0]) {
            Aperture recognizedAperture;
            ASSERT_FALSE(Unit::apertureFromFNumber(recognizedAperture, std::get<1>(readValue)))
                << "Problem: ‘f/" << std::get<1>(readValue) << "’ gives an overflow.";
            EXPECT_EQ(apexAperture, recognizedAperture);
        }
    }
}

TEST_F(ToStream, Aperture) {
    using flag = std::ios_base;

    std::wostringstream output;
    output.imbue(Unit::locale());
    output.fill(L'·');
    output.flags(flag::hex | flag::scientific | flag::showbase | flag::showpoint | flag::showpos | flag::uppercase);

    output.setf(flag::left, flag::adjustfield);
    output << std::setw(18) << Aperture(240) << L'|' << Aperture(120) << L'|' << std::setw(6) << Aperture(82)
           << std::flush;
    EXPECT_EQ(output.str(), L"f/10'4857'6·······|f/102'4|f/11'4") << "Left alignment, 18-character wide";
    EXPECT_EQ(output.width(), 0) << "Problem: width hasn't been reset";
    EXPECT_EQ(output.precision(), 6) << "Problem: precision has changed";
    output.str(std::wstring());

    output.setf(flag::internal, flag::adjustfield);
    output << std::setw(10) << Aperture(90) << L'|' << Aperture(30) << L'|' << std::setw(5) << Aperture(42)
           << std::flush;
    EXPECT_EQ(output.str(), L"····f/18'1|f/5.6|f/1'1") << "Justified alignment, 10-character wide";
    EXPECT_EQ(output.width(), 0) << "Problem: width hasn't been reset";
    EXPECT_EQ(output.precision(), 6) << "Problem: precision has changed";
    output.str(std::wstring());

    output.setf(flag::right, flag::adjustfield);
    output << std::setw(8) << Aperture(84) << L'|' << Aperture(32) << L'|' << std::setw(3) << Aperture(12)
           << std::flush;
    EXPECT_EQ(output.str(), L"··f/12'8|f/6.3|f/2") << "Right alignment, 8-character wide";
    EXPECT_EQ(output.width(), 0) << "Problem: width hasn't been reset";
    EXPECT_EQ(output.precision(), 6) << "Problem: precision has changed";

    EXPECT_EQ(output.flags(), flag::right | flag::hex | flag::scientific | flag::showbase | flag::showpoint |
                                  flag::showpos | flag::uppercase)
        << "Problem: flags have changed";
}
} // namespace Test

TEST(Comparators, Aperture) {
    Aperture F1_7, F5_6;

    ASSERT_FALSE(Unit::apertureFromFNumber(F1_7, 1.7)) << "Problem: ‘f/1.7’ gives an overflow.";
    ASSERT_FALSE(Unit::apertureFromFNumber(F5_6, 5.6)) << "Problem: ‘f/5.6’ gives an overflow.";

    ::Test::compare<Aperture>(F5_6, F1_7);
}
