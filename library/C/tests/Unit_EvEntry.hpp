/*
 * Copyright © 2019-2020 Thierry Simeao
 *
 * This file is part of Lampadaire cabalistique.
 *
 * Lampadaire cabalistique is free software: you can redistribute it and/or modify it under the terms of the GNU Affero
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * Lampadaire cabalistique is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License
 * for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with Lampadaire cabalistique.
 * If not, see <https://www.gnu.org/licenses/>.
 */
#pragma once
#include <gtest/gtest.h>

#include <bitset>
#include <iomanip>
#include <optional>

#include "Photo_EvData.hpp"

namespace Test {

template <typename StopSixth, typename TypeIn> class EvEntry {
public:
    class Option final {
    public:
        constexpr explicit Option(unsigned long long value) noexcept : options(value) {}

    private:
        constexpr explicit Option(const std::bitset<2> &&value) noexcept : options(value) {}

    public:
        Option   operator&(const Option &other) const noexcept { return Option(options & other.options); }
        Option   operator|(const Option &other) const noexcept { return Option(options | other.options); }
        explicit operator bool() const noexcept { return options.any(); }

    private:
        const std::bitset<2> options;
    };

    static constexpr Option STANDARD           = Option(0);
    static constexpr Option NON_STANDARD       = Option(1);
    static constexpr Option NEED_HALF_ROUNDING = Option(2);

protected:
    constexpr EvEntry(typename StopSixth::IntegerType expectedInternal, TypeIn entry, Option options,
                      bool skipHalfRounding) noexcept
        : expectedEv(expectedInternal), linearEntry(entry), testWithoutHalfRounding(!(options & NEED_HALF_ROUNDING)),
          testWithHalfRounding(!skipHalfRounding), testRoundTrip(!(options & NON_STANDARD)) {}

public:
    virtual ~EvEntry() {}
    template <typename TypeOut>
    void test(bool (*conversionInFctPtr)(StopSixth &, TypeIn, bool),
              std::optional<TypeOut> (StopSixth::*conversionOutFctPtr)() const, const char *prefix, const char *suffix,
              bool roundToHalf) const;
    template <typename TypeOut>
    void test(bool (*conversionInFctPtr)(StopSixth &, TypeIn, bool),
              std::optional<TypeOut> (StopSixth::*conversionOutFctPtr)() const, const char *prefix,
              const char *suffix) const;

public:
    const typename StopSixth::IntegerType expectedEv;
    const TypeIn                          linearEntry;
    const bool                            testWithoutHalfRounding; // don't force ½-stop
    const bool                            testWithHalfRounding;    // ½-stop forced
    const bool                            testRoundTrip;           // display → internal → display
};

template <typename StopSixth, typename TypeIn> class EvEntryFullStop : public EvEntry<StopSixth, TypeIn> {
public:
    EvEntryFullStop(
        int expected, TypeIn entry,
        typename EvEntry<StopSixth, TypeIn>::Option options = EvEntry<StopSixth, TypeIn>::STANDARD) noexcept;
};

template <typename StopSixth, typename TypeIn> class EvEntryPartialStop : public EvEntry<StopSixth, TypeIn> {
public:
    EvEntryPartialStop(
        typename StopSixth::IntegerType expected, TypeIn entry,
        typename EvEntry<StopSixth, TypeIn>::Option options = EvEntry<StopSixth, TypeIn>::STANDARD) noexcept;
};

template <typename StopSixth, typename TypeIn>
template <typename TypeOut>
void EvEntry<StopSixth, TypeIn>::test(bool (*conversionInFctPtr)(StopSixth &, TypeIn, bool),
                                      std::optional<TypeOut> (StopSixth::*conversionOutFctPtr)() const,
                                      const char *prefix, const char *suffix, bool roundToHalf) const {
    const char *ornament = (roundToHalf ? "’ (½-rounded)" : "’");
    StopSixth   apexValue;

    ASSERT_FALSE(conversionInFctPtr(apexValue, linearEntry, roundToHalf))
        << "Problem: ‘" << prefix << linearEntry << suffix << ornament << " gives an overflow.";
    EXPECT_EQ(static_cast<typename StopSixth::IntegerType>(apexValue), expectedEv)
        << "Problem: ‘" << prefix << linearEntry << suffix << ornament << std::setprecision(5)
        << " has been converted to ‘6 × " << static_cast<typename StopSixth::IntegerType>(apexValue) / 6.f
        << "’ instead of ‘6 × " << expectedEv / 6.f << "’.";

    if (nullptr != conversionOutFctPtr) {
        const std::optional<TypeOut> output = (apexValue.*conversionOutFctPtr)();

        ASSERT_TRUE(output.has_value()) << "Problem: ‘" << prefix << linearEntry << suffix << "’ gives no output.";
        EXPECT_EQ(static_cast<TypeIn>(*output), linearEntry)
            << "Problem: ‘" << prefix << linearEntry << suffix << "’ is displayed ‘" << prefix << *output << suffix
            << "’.";
    }
}

template <typename StopSixth, typename TypeIn>
template <typename TypeOut>
void EvEntry<StopSixth, TypeIn>::test(bool (*conversionInFctPtr)(StopSixth &, TypeIn, bool),
                                      std::optional<TypeOut> (StopSixth::*conversionOutFctPtr)() const,
                                      const char *prefix, const char *suffix) const {
    if (testWithoutHalfRounding) {
        test<TypeOut>(conversionInFctPtr, (testRoundTrip ? conversionOutFctPtr : nullptr), prefix, suffix, false);
    }
    if (testWithHalfRounding) {
        test<TypeOut>(conversionInFctPtr, nullptr, prefix, suffix, true);
    }
}

template <typename StopSixth, typename TypeIn>
EvEntryFullStop<StopSixth, TypeIn>::EvEntryFullStop(int expected, TypeIn entry,
                                                    typename EvEntry<StopSixth, TypeIn>::Option options) noexcept
    : EvEntry<StopSixth, TypeIn>(expected * 6, entry, options, false) {}

template <typename StopSixth, typename TypeIn>
EvEntryPartialStop<StopSixth, TypeIn>::EvEntryPartialStop(typename StopSixth::IntegerType expected, TypeIn entry,
                                                          typename EvEntry<StopSixth, TypeIn>::Option options) noexcept
    : EvEntry<StopSixth, TypeIn>(expected, entry, options, (expected % 3) != 0) {}

} // namespace Test
