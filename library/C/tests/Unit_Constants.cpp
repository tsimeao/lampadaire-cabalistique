/*
 * Copyright © 2019, 2021 Thierry Simeao
 *
 * This file is part of Lampadaire cabalistique.
 *
 * Lampadaire cabalistique is free software: you can redistribute it and/or modify it under the terms of the GNU Affero
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * Lampadaire cabalistique is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License
 * for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with Lampadaire cabalistique.
 * If not, see <https://www.gnu.org/licenses/>.
 */
#include <gtest/gtest.h>

#include <cmath>

#include "Photo_EvData.hpp"
#include "Unit.hpp"

namespace {

template <typename StopSixth> void testConstants(const char *className) {
    using IntegerType = typename StopSixth::IntegerType;

    constexpr IntegerType UNDEFINED = StopSixth::UNDEFINED;
    constexpr IntegerType MAXIMUM   = StopSixth::MAXIMUM;
    constexpr IntegerType MINIMUM   = StopSixth::MINIMUM;

    EXPECT_EQ(static_cast<IntegerType>(-UNDEFINED), UNDEFINED)
        << "‘−" << className << "::UNDEFINED’ ≠ ‘" << className << "::UNDEFINED’";
    EXPECT_EQ(static_cast<IntegerType>(-UNDEFINED), static_cast<IntegerType>(+UNDEFINED))
        << "‘−" << className << "::UNDEFINED’ ≠ ‘+" << className << "::UNDEFINED’";

    EXPECT_GE(MAXIMUM, MINIMUM) << "‘" << className << "::MAXIMUM’ < ‘" << className << "::MINIMUM’";
}

} // namespace

TEST(Constants, Unit) {
    EXPECT_EQ(Unit::PRINCIPAL_SQUARE_ROOT_OF_2, std::sqrt(2.L));
    EXPECT_EQ(Unit::PRINCIPAL_CUBE_ROOT_OF_2, std::cbrt(2.L));
    EXPECT_EQ(Unit::PRINCIPAL_4TH_ROOT_OF_2, std::exp2(0.25L));
    EXPECT_EQ(Unit::PRINCIPAL_6TH_ROOT_OF_2, std::exp2(1.L / 6.L));
    if constexpr (sizeof(long double) == 8) {
        EXPECT_EQ(Unit::TWO_RAISED_TO_THE_POWER_OF_2_3RD, std::exp2(2.L / 3.L) + /* rounding */ 0x1p-52L);
    } else {
        EXPECT_EQ(Unit::TWO_RAISED_TO_THE_POWER_OF_2_3RD, std::exp2(2.L / 3.L));
    }
    EXPECT_EQ(Unit::TWO_RAISED_TO_THE_POWER_OF_3_4TH, std::exp2(0.75L));
    EXPECT_EQ(Unit::TWO_RAISED_TO_THE_POWER_OF_5_6TH, std::exp2(5.L / 6.L));
}

TEST(Constants, StopSixth) {
    testConstants<Photo::EvData::Aperture>("Photo::EvData::Aperture");
    testConstants<Photo::EvData::ShutterSpeed>("Photo::EvData::ShutterSpeed");
    testConstants<Photo::EvData::Sensitivity>("Photo::EvData::Sensitivity");
}
