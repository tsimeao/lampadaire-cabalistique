/*
 * Copyright © 2019-2020 Thierry Simeao
 *
 * This file is part of Lampadaire cabalistique.
 *
 * Lampadaire cabalistique is free software: you can redistribute it and/or modify it under the terms of the GNU Affero
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * Lampadaire cabalistique is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License
 * for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with Lampadaire cabalistique.
 * If not, see <https://www.gnu.org/licenses/>.
 */
#include "Unit_EvEntry.hpp"

#include <cmath>
#include <cstdint>
#include <list>
#include <sstream>
#include <string>

#include "fixture/ToStream.hpp"
#include "fixture/ToString.hpp"

#include "Unit.hpp"
#include "comparators.hpp"

using Sensitivity = Photo::EvData::Sensitivity;
using IntegerType = Sensitivity::IntegerType;
using EvEntry     = Test::EvEntryPartialStop<Sensitivity, std::int_fast32_t>;
using FullEvEntry = Test::EvEntryFullStop<Sensitivity, std::int_fast32_t>;

namespace {

constexpr std::int_fast8_t MAXIMUM_DIN = Sensitivity::MAXIMUM / 2 + 6;
constexpr std::int_fast8_t MINIMUM_DIN = Sensitivity::MINIMUM / 2 + 6;

std::int_fast32_t dinToIso(std::int_fast32_t degrees) {
    return std::lround(std::pow(10.f, static_cast<float>(degrees - 1) / 10));
}

} // namespace

TEST(Input, Iso) {
    const std::list<FullEvEntry> iso_fullStop = {
        {0, 3},                              //
        {1, 6},                              //
        {2, 12},                             //
        {3, 25},                             //
        {4, 50},                             //
        {5, 100},                            //
        {6, 200},                            //
        {7, 400},                            //
        {8, 800},                            //
        {9, 1600},                           //
        {10, 3200},                          //
        {11, 6400},                          //
        {12, 12'500, EvEntry::NON_STANDARD}, //
        {12, 12'800, EvEntry::STANDARD},     //
        {13, 25'000, EvEntry::NON_STANDARD}, //
        {13, 25'600, EvEntry::STANDARD},     //
        {14, 50'000, EvEntry::NON_STANDARD}, //
        {14, 51'200, EvEntry::STANDARD},     //
        {15, 1'024'00},                      //
        {16, 2'048'00},                      //
        {17, 4'096'00},                      //
        {18, 8'192'00},                      //
        {19, 16'384'00},                     //
        {20, 32'768'00},                     //
        {21, 65'536'00},                     //
    };
    const std::list<EvEntry> iso_partialStop = {
        {2, 4},       //      3 +⅓
        {4, 5},       //      6 −⅓
                      //      6
        {8, 8},       //      6 +⅓
        {10, 10},     //     12 −⅓
                      //     12
        {14, 16},     //     12 +⅓
        {16, 20},     //     25 −⅓
                      //     25
        {20, 32},     //     25 +⅓
        {22, 40},     //     50 −⅓
                      //     50
        {26, 64},     //     50 +⅓
        {27, 70},     //         ½
        {28, 80},     //    100 −⅓
                      //    100
        {32, 125},    //    100 +⅓
        {33, 140},    //         ½
        {34, 160},    //    200 −⅓
                      //    200
        {38, 250},    //    200 +⅓
        {39, 280},    //         ½
        {40, 320},    //    400 −⅓
                      //    400
        {44, 500},    //    400 +⅓
        {45, 560},    //         ½
        {46, 640},    //    800 −⅓
                      //    800
        {50, 1000},   //    800 +⅓
        {51, 1100},   //         ½
        {52, 1250},   //   1600 −⅓
                      //   1600
        {56, 2000},   //   1600 +⅓
        {57, 2200},   //         ½
        {58, 2500},   //   3200 −⅓
                      //   3200
        {62, 4000},   //   3200 +⅓
        {63, 4500},   //         ½
        {64, 5000},   //   6400 −⅓
                      //   6400
        {68, 8000},   //   6400 +⅓
        {69, 9000},   //         ½
        {70, 10'000}, // 12,800 −⅓

        // bounds
        {Sensitivity::MINIMUM,
         static_cast<std::int_fast32_t>(std::lround(std::exp2(static_cast<float>(Sensitivity::MINIMUM) / 6) * 3.125f)),
         EvEntry::NON_STANDARD},
        {Sensitivity::MAXIMUM,
         static_cast<std::int_fast32_t>(std::lround(std::exp2(static_cast<float>(Sensitivity::MAXIMUM) / 6) * 3.125f)),
         EvEntry::NON_STANDARD},
    };

    for (auto sensitivity : iso_fullStop) {
        sensitivity.test(&Unit::sensitivityFromIso, &Sensitivity::toIso, "", " iso");
    }

    for (auto sensitivity : iso_partialStop) {
        sensitivity.test(&Unit::sensitivityFromIso, &Sensitivity::toIso, "", " iso");
    }

    // limits
    Sensitivity apexSensitivity;

    apexSensitivity = Sensitivity(0);
    EXPECT_TRUE(Unit::sensitivityFromIso(apexSensitivity, 0)) << "Problem: ‘0 iso’ doesn't give an overflow.";
    EXPECT_EQ(static_cast<IntegerType>(apexSensitivity), Sensitivity::MINIMUM)
        << "Problem: ‘0 iso’ has been converted to ‘" << static_cast<IntegerType>(apexSensitivity)
        << "’ instead of ‘Sensitivity::MINIMUM’.";

    apexSensitivity = Sensitivity(0);
    Unit::sensitivityFromIso(apexSensitivity, INT32_MAX);
    EXPECT_EQ(static_cast<IntegerType>(apexSensitivity), Sensitivity::MAXIMUM)
        << "Problem: ‘INT32_MAX iso’ has been converted to ‘" << static_cast<IntegerType>(apexSensitivity)
        << "’ instead of ‘Sensitivity::MAXIMUM’.";
}

TEST(Output, Iso) {
    static constexpr std::int_fast32_t EXPECTED_ISO[16] = {
        1, 1, 1, 1,    // -10 → -7
        2, 2, 2, 2, 2, //  -6 → -2
        3, 3,          //  -1 →  0
        4, 4, 4,       //  +1 → +3
        5,             //  +4
        6,             //  +5
    };

    std::optional<std::int_fast32_t> iso;
    std::int_fast32_t                expectedIso = -1;

    static_assert(Sensitivity::MINIMUM >= -10);
    for (IntegerType internal = Sensitivity::MINIMUM; internal < 6; ++internal) {
        iso = Sensitivity(internal).toIso();
        ASSERT_TRUE(iso.has_value()) << "Problem: ‘Sensitivity(" << internal << ")’ gives no output.";
        EXPECT_EQ(*iso, EXPECTED_ISO[internal + 10])
            << "Problem: " << internal << " is displayed ‘" << *iso << " iso’ instead of ‘"
            << (EXPECTED_ISO + 10)[internal] << " iso’.";
    }

    for (IntegerType internal = 7; internal <= Sensitivity::MAXIMUM; internal += 2) {
        if (const IntegerType subStop = internal % 6; subStop == 3) {
            continue;
        }

        iso         = Sensitivity(internal).toIso();
        expectedIso = std::lround(std::exp2(internal / 6.0) * 3.125);
        ASSERT_TRUE(iso.has_value()) << "Problem: ‘Sensitivity(" << internal << ")’ gives no output.";
        EXPECT_EQ(*iso, expectedIso) << "Problem: " << internal << " is displayed ‘" << *iso << " iso’ instead of ‘"
                                     << expectedIso << " iso’.";
    }
}

TEST(RoundTrip, Iso) {
    struct {
        Sensitivity source, result;
    } internal;
    std::optional<std::int_fast32_t> iso;

    for (std::int_fast32_t expectedIso = 1; expectedIso <= 12; ++expectedIso) {
        ASSERT_FALSE(Unit::sensitivityFromIso(internal.result, expectedIso))
            << "Problem: ‘" << expectedIso << " iso’ gives an overflow.";
        iso = internal.result.toIso();
        ASSERT_TRUE(iso.has_value()) << "Problem: ‘" << expectedIso << " iso’ gives no output.";
        EXPECT_EQ(*iso, expectedIso) << "Problem: ‘" << expectedIso << " iso’ → "
                                     << static_cast<IntegerType>(internal.result) << " → ‘" << *iso << " iso’";
    }

    for (IntegerType i = 6; i <= Sensitivity::MAXIMUM; ++i) {
        internal.source = Sensitivity(i);
        internal.result = Sensitivity(Sensitivity::UNDEFINED);

        iso = internal.source.toIso();
        ASSERT_TRUE(iso.has_value()) << "Problem: ‘Sensitivity(" << i << ")’ gives no output.";
        ASSERT_FALSE(Unit::sensitivityFromIso(internal.result, *iso))
            << "Problem: ‘" << *iso << " iso’ gives an overflow.";
        EXPECT_EQ(static_cast<IntegerType>(internal.source), static_cast<IntegerType>(internal.result))
            << "Problem: " << static_cast<IntegerType>(internal.source) << " → ‘" << *iso << " iso’ → "
            << static_cast<IntegerType>(internal.result);
    }
}

TEST(Input, Din) {
    struct {
        Sensitivity fromIso;
        Sensitivity fromDin;
    } Sv;

    ASSERT_FALSE(Unit::sensitivityFromIso(Sv.fromIso, 100));
    ASSERT_FALSE(Unit::sensitivityFromDin(Sv.fromDin, 21));
    EXPECT_EQ(static_cast<IntegerType>(Sv.fromDin), static_cast<IntegerType>(Sv.fromIso));

    ASSERT_FALSE(Unit::sensitivityFromIso(Sv.fromIso, 400));
    ASSERT_FALSE(Unit::sensitivityFromDin(Sv.fromDin, 27));
    EXPECT_EQ(static_cast<IntegerType>(Sv.fromDin), static_cast<IntegerType>(Sv.fromIso));

    ASSERT_FALSE(Unit::sensitivityFromIso(Sv.fromIso, 800));
    ASSERT_FALSE(Unit::sensitivityFromDin(Sv.fromDin, 30));
    EXPECT_EQ(static_cast<IntegerType>(Sv.fromDin), static_cast<IntegerType>(Sv.fromIso));

    ASSERT_FALSE(Unit::sensitivityFromDin(Sv.fromDin, MINIMUM_DIN));
    ASSERT_FALSE(Unit::sensitivityFromIso(Sv.fromIso, dinToIso(MINIMUM_DIN)));
    EXPECT_EQ(static_cast<IntegerType>(Sv.fromDin), static_cast<IntegerType>(Sv.fromIso));

    ASSERT_FALSE(Unit::sensitivityFromDin(Sv.fromDin, MAXIMUM_DIN));
    ASSERT_FALSE(Unit::sensitivityFromIso(Sv.fromIso, dinToIso(MAXIMUM_DIN)));
    EXPECT_EQ(static_cast<IntegerType>(Sv.fromDin), static_cast<IntegerType>(Sv.fromIso));

    // limits
    Sensitivity apexSensitivity;

    apexSensitivity = Sensitivity(0);
    EXPECT_TRUE(Unit::sensitivityFromDin(apexSensitivity, MINIMUM_DIN - 1))
        << "Problem: ‘" << MINIMUM_DIN - 1 << "°’ (MINIMUM_DIN - 1) doesn't give an overflow.";
    EXPECT_EQ(static_cast<IntegerType>(apexSensitivity), Sensitivity::MINIMUM)
        << "Problem: ‘" << MINIMUM_DIN - 1 << "°’ (MINIMUM_DIN - 1) has been converted to ‘"
        << static_cast<IntegerType>(apexSensitivity) << "’ instead of ‘Sensitivity::MINIMUM’.";

    apexSensitivity = Sensitivity(0);
    EXPECT_TRUE(Unit::sensitivityFromDin(apexSensitivity, MAXIMUM_DIN + 1))
        << "Problem: ‘" << MAXIMUM_DIN + 1 << "°’ (MAXIMUM_DIN + 1) doesn't give an overflow.";
    EXPECT_EQ(static_cast<IntegerType>(apexSensitivity), Sensitivity::MAXIMUM)
        << "Problem: ‘" << MAXIMUM_DIN + 1 << "°’ (MAXIMUM_DIN + 1) has been converted to ‘"
        << static_cast<IntegerType>(apexSensitivity) << "’ instead of ‘Sensitivity::MAXIMUM’.";
}

TEST(Output, Din) {
    IntegerType          internal;
    std::int_fast8_t     integerDin;
    std::optional<float> din;
    float                expectedDin = NAN;

    for (internal = Sensitivity::MINIMUM | 1, integerDin = MINIMUM_DIN; internal <= Sensitivity::MAXIMUM;
         internal += 2, ++integerDin) {
        din         = Sensitivity(internal).toDin();
        expectedDin = integerDin + 0.5f;
        ASSERT_TRUE(din.has_value()) << "Problem: ‘Sensitivity(" << internal << ")’ gives no output.";
        EXPECT_EQ(*din, expectedDin) << "Problem: " << internal << " is displayed ‘" << *din << "°’ instead of ‘"
                                     << expectedDin << "°’.";
    }
}

TEST(RoundTrip, Din) {
    struct {
        std::int_fast32_t    source;
        std::optional<float> result;
    } degrees;
    Sensitivity internal;

    for (degrees.source = MINIMUM_DIN; degrees.source <= MAXIMUM_DIN; ++degrees.source) {
        ASSERT_FALSE(Unit::sensitivityFromDin(internal, degrees.source))
            << "Problem: ‘" << degrees.source << "°’ gives an overflow.";
        degrees.result = internal.toDin();
        ASSERT_TRUE(degrees.result.has_value()) << "Problem: ‘" << degrees.source << "°’ gives no output.";
        EXPECT_EQ(degrees.source, *degrees.result)
            << "Problem: ‘" << degrees.source << "°’ → " << static_cast<IntegerType>(internal) << " → ‘"
            << *degrees.result << "°’";
    }
}

namespace Test {
TEST_F(ToString, Sensitivity) {
    static constexpr const wchar_t *EXPECTED_OUTPUT[16] = {
        L"ISO 1/1°",   L"ISO 1/1.5°", L"ISO 1/2°",   L"ISO 1/2.5°",              // -10 → -7
        L"ISO 2/3°",   L"ISO 2/3.5°", L"ISO 2/4°",   L"ISO 2/4.5°", L"ISO 2/5°", //  -6 → -2
        L"ISO 3/5.5°", L"ISO 3",                                                 //  -1 →  0
        L"ISO 4/6.5°", L"ISO 4/7°",   L"ISO 4/7.5°",                             //  +1 → +3
        L"ISO 5",                                                                //  +4
        L"ISO 6/8.5°",                                                           //  +5
    };

    setExtractionLocale(Unit::locale());
    setRegularExpression(L"ISO ([1-9]\\d*(?:'\\d+)*)(?:/([1-9]\\d*(?:\\.5)?)°)?");

    static_assert(Sensitivity::MINIMUM >= -10);
    for (IntegerType internal = Sensitivity::MINIMUM; internal <= 5; ++internal) {
        EXPECT_EQ(static_cast<std::wstring>(Sensitivity(internal)), EXPECTED_OUTPUT[internal + 10]);
    }

    Sensitivity apexSensitivity;
    for (IntegerType internal = 6; internal <= Sensitivity::MAXIMUM; ++internal) {
        apexSensitivity = Sensitivity(internal);
        std::tuple<std::bitset<2>, std::int_fast32_t, float> readValue =
            match<std::int_fast32_t, float>(static_cast<std::wstring>(apexSensitivity), L"a sensitivity");

        if (std::get<0>(readValue)[1]) {
            ADD_FAILURE() << "Problem: degrees are uselessly displayed: ‘ISO " << std::get<1>(readValue) << "\033[31m/"
                          << std::get<2>(readValue) << "°\033[0m’.";

            Sensitivity recognizedDin;
            ASSERT_FALSE(Unit::sensitivityFromDin(recognizedDin, std::get<2>(readValue)))
                << "Problem: ‘ISO " << std::get<2>(readValue) << "°’ gives an overflow.";
            EXPECT_EQ(recognizedDin, apexSensitivity);
        }

        if (std::get<0>(readValue)[0]) {
            Sensitivity recognizedIso;
            ASSERT_FALSE(Unit::sensitivityFromIso(recognizedIso, std::get<1>(readValue)))
                << "Problem: ‘ISO " << std::get<1>(readValue) << "’ gives an overflow.";
            EXPECT_EQ(recognizedIso, apexSensitivity);
        }
    }
}

TEST_F(ToStream, Sensitivity) {
    using flag = std::ios_base;

    std::wostringstream output;
    output.imbue(Unit::locale());
    output.fill(L'·');
    output.flags(flag::hex | flag::scientific | flag::showbase | flag::showpoint | flag::showpos);

    output.setf(flag::left, flag::adjustfield);
    output << std::setw(22) << Sensitivity(150) << L'|' << Sensitivity(90) << L'|' << std::setw(8) << Sensitivity(30)
           << std::flush;
    EXPECT_EQ(output.str(), L"ISO 1048'5760'0·······|ISO 1'0240'0|ISO 10'0") << "Left alignment, 22-character wide";
    EXPECT_EQ(output.width(), 0) << "Problem: width hasn't been reset";
    EXPECT_EQ(output.precision(), 6) << "Problem: precision has changed";
    output.str(std::wstring());

    output.setf(flag::internal, flag::adjustfield);
    output << std::setw(16) << Sensitivity(46) << L'|' << Sensitivity(48) << L'|' << std::setw(7) << Sensitivity(10)
           << std::flush;
    EXPECT_EQ(output.str(), L"ISO         64'0|ISO 80'0|ISO 1'0") << "Justified alignment, 16-character wide";
    EXPECT_EQ(output.width(), 0) << "Problem: width hasn't been reset";
    EXPECT_EQ(output.precision(), 6) << "Problem: precision has changed";
    output.str(std::wstring());

    output.setf(flag::right, flag::adjustfield);
    output << std::setw(10) << Sensitivity(30) << L'|' << Sensitivity(26) << L'|' << std::setw(5) << Sensitivity(8)
           << std::flush;
    EXPECT_EQ(output.str(), L"··ISO 10'0|ISO 6'4|ISO 8") << "Right alignment, 10-character wide";
    EXPECT_EQ(output.width(), 0) << "Problem: width hasn't been reset";
    EXPECT_EQ(output.precision(), 6) << "Problem: precision has changed";

    EXPECT_EQ(output.flags(),
              flag::right | flag::hex | flag::scientific | flag::showbase | flag::showpoint | flag::showpos)
        << "Problem: flags have changed";
}
} // namespace Test

TEST(Comparators, Sensitivity) {
    Sensitivity ISO100, ISO800;

    ASSERT_FALSE(Unit::sensitivityFromIso(ISO100, 100)) << "Problem: ‘100 iso’ gives an overflow.";
    ASSERT_FALSE(Unit::sensitivityFromIso(ISO800, 800)) << "Problem: ‘800 iso’ gives an overflow.";

    ::Test::compare<Sensitivity>(ISO100, ISO800);
}
