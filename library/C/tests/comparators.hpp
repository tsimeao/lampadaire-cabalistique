/*
 * Copyright ©2019 Thierry Simeao
 *
 * This file is part of Lampadaire cabalistique.
 *
 * Lampadaire cabalistique is free software: you can redistribute it and/or modify it under the terms of the GNU Affero
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * Lampadaire cabalistique is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License
 * for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with Lampadaire cabalistique.
 * If not, see <https://www.gnu.org/licenses/>.
 */
#pragma once
#include <gtest/gtest.h>

namespace Test {

template <typename T> void compare(const T &smaller, const T &bigger) {
    const T undefined;

    ASSERT_FALSE(static_cast<bool>(undefined));
    ASSERT_TRUE(static_cast<bool>(smaller));
    ASSERT_TRUE(static_cast<bool>(bigger));
    ASSERT_LT(smaller, bigger);

    EXPECT_TRUE(!undefined);
    EXPECT_FALSE(!bigger);

    EXPECT_TRUE(bigger == bigger);
    EXPECT_FALSE(bigger == smaller);

    EXPECT_FALSE(bigger != bigger);
    EXPECT_TRUE(bigger != smaller);

    EXPECT_TRUE(smaller < bigger);
    EXPECT_FALSE(smaller < smaller);
    EXPECT_FALSE(bigger < smaller);

    EXPECT_TRUE(bigger > smaller);
    EXPECT_FALSE(bigger > bigger);
    EXPECT_FALSE(smaller > bigger);

    EXPECT_TRUE(smaller <= bigger);
    EXPECT_TRUE(smaller <= smaller);
    EXPECT_FALSE(bigger <= smaller);

    EXPECT_TRUE(bigger >= smaller);
    EXPECT_TRUE(bigger >= bigger);
    EXPECT_FALSE(smaller >= bigger);

    EXPECT_FALSE(undefined == bigger);
    EXPECT_TRUE(undefined != bigger);
    EXPECT_FALSE(undefined < bigger);
    EXPECT_FALSE(undefined > bigger);
    EXPECT_FALSE(undefined <= bigger);
    EXPECT_FALSE(undefined >= bigger);

    EXPECT_FALSE(bigger == undefined);
    EXPECT_TRUE(bigger != undefined);
    EXPECT_FALSE(bigger < undefined);
    EXPECT_FALSE(bigger > undefined);
    EXPECT_FALSE(bigger <= undefined);
    EXPECT_FALSE(bigger >= undefined);

    EXPECT_FALSE(undefined == undefined);
    EXPECT_TRUE(undefined != undefined);
    EXPECT_FALSE(undefined < undefined);
    EXPECT_FALSE(undefined > undefined);
    EXPECT_FALSE(undefined <= undefined);
    EXPECT_FALSE(undefined >= undefined);
}

} // namespace Test
