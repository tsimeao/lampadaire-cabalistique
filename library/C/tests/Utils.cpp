/*
 * Copyright ©2020 Thierry Simeao
 *
 * This file is part of Lampadaire cabalistique.
 *
 * Lampadaire cabalistique is free software: you can redistribute it and/or modify it under the terms of the GNU Affero
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * Lampadaire cabalistique is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License
 * for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with Lampadaire cabalistique.
 * If not, see <https://www.gnu.org/licenses/>.
 */
#include <gtest/gtest.h>

#include <cstdint>

#include "Utils.hpp"

namespace {

template <typename Integer, int Minimum, int Maximum, bool expected> void expectInRange(Integer number) {
    auto fctIsInRange = [=]() -> bool { return Utils::isInRange<Minimum, Maximum>(number); };

    if constexpr (expected) {
        EXPECT_TRUE(fctIsInRange()) << "Problem: ‘Utils::isInRange<" << Minimum << ", " << Maximum << ">("
                                    << static_cast<int>(number) << ")’ returns ‘false’ which means “"
                                    << static_cast<int>(number) << " ∉ [" << Minimum << ", " << Maximum << "]”.";
    } else {
        EXPECT_FALSE(fctIsInRange()) << "Problem: ‘Utils::isInRange<" << Minimum << ", " << Maximum << ">("
                                     << static_cast<int>(number) << ")’ returns ‘true’ which means “"
                                     << static_cast<int>(number) << " ∈ [" << Minimum << ", " << Maximum << "]”.";
    }
}

} // namespace

TEST(Utils, isInRange) {
    expectInRange<int8_t, -20, -10, false>(-128);
    expectInRange<int8_t, -20, -10, false>(-21);
    expectInRange<int8_t, -20, -10, true>(-20);
    expectInRange<int8_t, -20, -10, true>(-15);
    expectInRange<int8_t, -20, -10, true>(-10);
    expectInRange<int8_t, -20, -10, false>(-9);
    expectInRange<int8_t, -20, -10, false>(0);
    expectInRange<int8_t, -20, -10, false>(1);
    expectInRange<int8_t, -20, -10, false>(127);

    expectInRange<int8_t, -10, +10, false>(-128);
    expectInRange<int8_t, -10, +10, false>(-11);
    expectInRange<int8_t, -10, +10, true>(-10);
    expectInRange<int8_t, -10, +10, true>(0);
    expectInRange<int8_t, -10, +10, true>(+10);
    expectInRange<int8_t, -10, +10, false>(+11);
    expectInRange<int8_t, -10, +10, false>(+127);

    expectInRange<int8_t, +10, +20, false>(-128);
    expectInRange<int8_t, +10, +20, false>(-1);
    expectInRange<int8_t, +10, +20, false>(0);
    expectInRange<int8_t, +10, +20, false>(9);
    expectInRange<int8_t, +10, +20, true>(10);
    expectInRange<int8_t, +10, +20, true>(15);
    expectInRange<int8_t, +10, +20, true>(20);
    expectInRange<int8_t, +10, +20, false>(21);
    expectInRange<int8_t, +10, +20, false>(127);

    expectInRange<uint8_t, 10, 20, false>(0);
    expectInRange<uint8_t, 10, 20, false>(9);
    expectInRange<uint8_t, 10, 20, true>(10);
    expectInRange<uint8_t, 10, 20, true>(15);
    expectInRange<uint8_t, 10, 20, true>(20);
    expectInRange<uint8_t, 10, 20, false>(21);
    expectInRange<uint8_t, 10, 20, false>(255);
}
