/*
 * Copyright © 2019-2020 Thierry Simeao
 *
 * This file is part of Lampadaire cabalistique.
 *
 * Lampadaire cabalistique is free software: you can redistribute it and/or modify it under the terms of the GNU Affero
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * Lampadaire cabalistique is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License
 * for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with Lampadaire cabalistique.
 * If not, see <https://www.gnu.org/licenses/>.
 */
#include "Unit_EvEntry.hpp"

#include <cmath>
#include <list>
#include <sstream>
#include <string>

#include "fixture/ToStream.hpp"
#include "fixture/ToString.hpp"

#include "Unit.hpp"
#include "comparators.hpp"

using ShutterSpeed = Photo::EvData::ShutterSpeed;
using IntegerType  = ShutterSpeed::IntegerType;
using EvEntry      = Test::EvEntryPartialStop<ShutterSpeed, float>;
using FullEvEntry  = Test::EvEntryFullStop<ShutterSpeed, float>;

TEST(Input, reverseSeconds) {
    const std::list<FullEvEntry> fractionsOfASecond_fullStop = {
        {0, 1},       //
        {1, 2},       //
        {2, 4},       //
        {3, 8},       //
        {4, 15},      //
        {5, 30},      //
        {6, 60},      //
        {7, 125},     //
        {8, 250},     //
        {9, 500},     //
        {10, 1000},   //
        {11, 2000},   //
        {12, 4000},   //
        {13, 8000},   //
        {14, 16'000}, //
    };
    const std::list<EvEntry> fractionsOfASecond_partialStop = {
        {2, 1.3},                                                        // 1 s      −⅓
        {3, 1.4, EvEntry::STANDARD},                                     //           ½
        {3, 1.5, EvEntry::NON_STANDARD | EvEntry::NEED_HALF_ROUNDING},   //           ½
        {4, 1.6},                                                        // 1/2      +⅓
                                                                         // 1/2
        {8, 2.5},                                                        // 1/2      −⅓
        {9, 2.8, EvEntry::STANDARD},                                     //           ½
        {9, 3, EvEntry::NON_STANDARD | EvEntry::NEED_HALF_ROUNDING},     //           ½
        {10, 3},                                                         // 1/4      +⅓
                                                                         // 1/4
        {14, 5},                                                         // 1/4      −⅓
        {15, 5.6, EvEntry::STANDARD},                                    //           ½
        {15, 6, EvEntry::NON_STANDARD | EvEntry::NEED_HALF_ROUNDING},    //           ½
        {16, 6},                                                         // 1/8      +⅓
                                                                         // 1/8
        {20, 10},                                                        // 1/8      −⅓
        {21, 10, EvEntry::NON_STANDARD | EvEntry::NEED_HALF_ROUNDING},   //           ½
        {21, 11, EvEntry::STANDARD},                                     //           ½
        {22, 13},                                                        // 1/15     +⅓
                                                                         // 1/15
        {26, 20},                                                        // 1/15     −⅓
        {27, 20, EvEntry::NON_STANDARD | EvEntry::NEED_HALF_ROUNDING},   //           ½
        {27, 22, EvEntry::STANDARD},                                     //           ½
        {28, 25},                                                        // 1/30     +⅓
                                                                         // 1/30
        {32, 40},                                                        // 1/30     −⅓
        {33, 45},                                                        //           ½
        {34, 50},                                                        // 1/60     +⅓
                                                                         // 1/60
        {38, 80},                                                        // 1/60     −⅓
        {39, 90},                                                        //           ½
        {40, 100},                                                       // 1/125    +⅓
                                                                         // 1/125
        {44, 160},                                                       // 1/125    −⅓
        {45, 180},                                                       //           ½
        {46, 200},                                                       // 1/250    +⅓
                                                                         // 1/250
        {50, 320},                                                       // 1/250    −⅓
        {51, 350},                                                       //           ½
        {52, 400},                                                       // 1/500    +⅓
                                                                         // 1/500
        {56, 640},                                                       // 1/500    −⅓
        {57, 700, EvEntry::STANDARD},                                    //           ½
        {57, 750, EvEntry::NON_STANDARD | EvEntry::NEED_HALF_ROUNDING},  //           ½
        {58, 800},                                                       // 1/1000   +⅓
                                                                         // 1/1000
        {62, 1250},                                                      // 1/1000   −⅓
        {63, 1400, EvEntry::STANDARD},                                   //           ½
        {63, 1500, EvEntry::NON_STANDARD | EvEntry::NEED_HALF_ROUNDING}, //           ½
        {64, 1600},                                                      // 1/2000   +⅓
                                                                         // 1/2000
        {68, 2500},                                                      // 1/2000   −⅓
        {69, 2800, EvEntry::STANDARD},                                   //           ½
        {69, 3000, EvEntry::NON_STANDARD | EvEntry::NEED_HALF_ROUNDING}, //           ½
        {70, 3200},                                                      // 1/4000   +⅓
                                                                         // 1/4000
        {74, 5000},                                                      // 1/4000   −⅓
        {75, 5600, EvEntry::STANDARD},                                   //           ½
        {75, 6000, EvEntry::NON_STANDARD | EvEntry::NEED_HALF_ROUNDING}, //           ½
        {76, 6400},                                                      // 1/8000   +⅓
                                                                         // 1/8000
        {80, 10'000},                                                    // 1/8000   −⅓
        {81, 11'000},                                                    //           ½
        {82, 12'500},                                                    // 1/16,000 +⅓
                                                                         // 1/16,000
        {86, 20'000},                                                    // 1/16,000 −⅓

        // bounds
        {ShutterSpeed::MINIMUM,
         static_cast<float>(std::pow(10, static_cast<double>(ShutterSpeed::MINIMUM) / (6 * 10 / 3))),
         EvEntry::NON_STANDARD},
        {ShutterSpeed::MAXIMUM,
         static_cast<float>(std::pow(10, static_cast<double>(ShutterSpeed::MAXIMUM) / (6 * 10 / 3))),
         EvEntry::NON_STANDARD},
    };

    for (auto speed : fractionsOfASecond_fullStop) {
        speed.test(&Unit::shutterSpeedFromReverseSeconds, &ShutterSpeed::toReverseSeconds, "1/", "");
    }

    for (auto speed : fractionsOfASecond_partialStop) {
        speed.test(&Unit::shutterSpeedFromReverseSeconds, &ShutterSpeed::toReverseSeconds, "1/", "");
    }

    // limits
    ShutterSpeed apexShutterSpeed;

    apexShutterSpeed = ShutterSpeed(0);
    EXPECT_TRUE(Unit::shutterSpeedFromReverseSeconds(apexShutterSpeed, 0))
        << "Problem: ‘1/0’ doesn't give an overflow.";
    EXPECT_EQ(static_cast<IntegerType>(apexShutterSpeed), ShutterSpeed::MINIMUM)
        << "Problem: ‘1/0’ has been converted to ‘" << static_cast<IntegerType>(apexShutterSpeed)
        << "’ instead of ‘ShutterSpeed::MINIMUM’.";

    apexShutterSpeed = ShutterSpeed(0);
    EXPECT_TRUE(Unit::shutterSpeedFromReverseSeconds(apexShutterSpeed, INFINITY))
        << "Problem: ‘1/∞’ doesn't give an overflow.";
    EXPECT_EQ(static_cast<IntegerType>(apexShutterSpeed), ShutterSpeed::MAXIMUM)
        << "Problem: ‘1/∞’ has been converted to ‘" << static_cast<IntegerType>(apexShutterSpeed)
        << "’ instead of ‘ShutterSpeed::MAXIMUM’.";
}

TEST(Output, reverseSeconds) {
    std::optional<double> speed;
    double                expectedReverseSeconds = NAN;

    for (IntegerType internal = ShutterSpeed::MINIMUM | 1; internal <= ShutterSpeed::MAXIMUM; internal += 2) {
        if (const IntegerType subStop = internal % 6; subStop == 3 || subStop == -3) {
            continue;
        }

        speed                  = ShutterSpeed(internal).toReverseSeconds();
        expectedReverseSeconds = std::pow(10, internal / 20.0);

        ASSERT_TRUE(speed.has_value()) << "Problem: ‘ShutterSpeed(" << internal << ")’ gives no output.";
        EXPECT_EQ(*speed, expectedReverseSeconds)
            << std::setprecision(10) << "Problem: " << internal << " is displayed ‘1/" << *speed << "’ instead of ‘1/"
            << expectedReverseSeconds << "’.";
    }
}

TEST(RoundTrip, reverseSeconds) {
    struct {
        ShutterSpeed source, result;
    } internal;
    std::optional<double> speed;

    for (IntegerType i = ShutterSpeed::MINIMUM; i <= ShutterSpeed::MAXIMUM; ++i) {
        internal.source = ShutterSpeed(i);
        internal.result = ShutterSpeed(ShutterSpeed::UNDEFINED);

        speed = internal.source.toReverseSeconds();
        ASSERT_TRUE(speed.has_value()) << "Problem: ‘ShutterSpeed(" << i << ")’ gives no output.";
        ASSERT_FALSE(Unit::shutterSpeedFromReverseSeconds(internal.result, *speed))
            << "Problem: ‘1/" << *speed << "’ gives an overflow.";
        EXPECT_EQ(static_cast<IntegerType>(internal.source), static_cast<IntegerType>(internal.result))
            << "Problem: " << static_cast<IntegerType>(internal.source) << " → ‘1/" << *speed << "’ → "
            << static_cast<IntegerType>(internal.result);
    }
}

TEST(Input, seconds) {
    const std::list<FullEvEntry> seconds_fullStop = {
        {1, 0.5},                                                            // 1/2
        {0, 1},                                                              //
        {-1, 2},                                                             //
        {-2, 4},                                                             //
        {-3, 8},                                                             //
        {-4, 15},                                                            //
        {-5, 30},                                                            //
        {-6, 60},                                                            //  1 min
        {-7, 2 * 60, EvEntry::NON_STANDARD},                                 //  2 min
        {-7, 125, EvEntry::STANDARD},                                        //  2 min
        {-8, 4 * 60, EvEntry::NON_STANDARD},                                 //  4 min
        {-8, 250, EvEntry::STANDARD},                                        //  4 min
        {-9, 8 * 60, EvEntry::NON_STANDARD},                                 //  8 min
        {-9, 500, EvEntry::STANDARD},                                        //  8 min
        {-10, 15 * 60, EvEntry::NON_STANDARD | EvEntry::NEED_HALF_ROUNDING}, // 15 min
        {-10, 1000, EvEntry::STANDARD},                                      // 15 min
        {-11, 30 * 60, EvEntry::NON_STANDARD | EvEntry::NEED_HALF_ROUNDING}, // 30 min
        {-11, 2000, EvEntry::STANDARD},                                      // 30 min
    };
    const std::list<EvEntry> seconds_partialStop = {
        {10, 0.3, EvEntry::NON_STANDARD},                                 // 1/4    +⅓
        {9, 0.35},                                                        //         ½
        {8, 0.4, EvEntry::NON_STANDARD},                                  // 1/2    −⅓
                                                                          // 1/2
        {4, 0.6, EvEntry::NON_STANDARD},                                  // 1/2    +⅓
        {3, 0.7},                                                         //         ½
        {2, 0.8, EvEntry::NON_STANDARD},                                  //  1 s   −⅓
                                                                          //  1 s
        {-2, 1.3},                                                        //  1 s   +⅓
        {-3, 1.4, EvEntry::STANDARD},                                     //         ½
        {-3, 1.5, EvEntry::NON_STANDARD | EvEntry::NEED_HALF_ROUNDING},   //         ½
        {-4, 1.6},                                                        //  2 s   −⅓
                                                                          //  2 s
        {-8, 2.5},                                                        //  2 s   +⅓
        {-9, 2.8, EvEntry::STANDARD},                                     //         ½
        {-9, 3, EvEntry::NON_STANDARD | EvEntry::NEED_HALF_ROUNDING},     //         ½
        {-10, 3, EvEntry::STANDARD},                                      //  4 s   −⅓
        {-10, 3.2, EvEntry::NON_STANDARD},                                //  4 s   −⅓
                                                                          //  4 s
        {-14, 5},                                                         //  4 s   +⅓
        {-15, 5.6, EvEntry::STANDARD},                                    //         ½
        {-15, 6, EvEntry::NON_STANDARD | EvEntry::NEED_HALF_ROUNDING},    //         ½
        {-16, 6},                                                         //  8 s   −⅓
                                                                          //  8 s
        {-20, 10},                                                        //  8 s   +⅓
        {-21, 10, EvEntry::NON_STANDARD | EvEntry::NEED_HALF_ROUNDING},   //         ½
        {-21, 11, EvEntry::STANDARD},                                     //         ½
        {-22, 12, EvEntry::NON_STANDARD},                                 // 15 s   −⅓
        {-22, 13, EvEntry::STANDARD},                                     // 15 s   −⅓
                                                                          // 15 s
        {-26, 20},                                                        // 15 s   +⅓
        {-27, 20, EvEntry::NON_STANDARD | EvEntry::NEED_HALF_ROUNDING},   //         ½
        {-27, 22, EvEntry::STANDARD},                                     //         ½
        {-28, 24, EvEntry::NON_STANDARD},                                 // 30 s   −⅓
        {-28, 25, EvEntry::STANDARD},                                     // 30 s   −⅓
                                                                          // 30 s
        {-32, 40},                                                        // 30 s   +⅓
        {-33, 45},                                                        //         ½
        {-34, 50},                                                        //  1 min −⅓
                                                                          //  1 min
        {-38, 80},                                                        //  1 min +⅓
        {-39, 90},                                                        //         ½
        {-40, 100},                                                       //  2 min −⅓
                                                                          //  2 min
        {-44, 160},                                                       //  2 min +⅓
        {-45, 180},                                                       //         ½
        {-46, 200},                                                       //  4 min −⅓
                                                                          //  4 min
        {-50, 320},                                                       //  4 min +⅓
        {-51, 350},                                                       //         ½
        {-52, 400},                                                       //  8 min −⅓
                                                                          //  8 min
        {-56, 640},                                                       //  8 min +⅓
        {-57, 700, EvEntry::STANDARD},                                    //         ½
        {-57, 750, EvEntry::NON_STANDARD | EvEntry::NEED_HALF_ROUNDING},  //         ½
        {-58, 800},                                                       // 15 min −⅓
                                                                          // 15 min
        {-62, 1250},                                                      // 15 min +⅓
        {-63, 1400, EvEntry::STANDARD},                                   //         ½
        {-63, 1500, EvEntry::NON_STANDARD | EvEntry::NEED_HALF_ROUNDING}, //         ½
        {-64, 1600},                                                      // 30 min −⅓

        // bounds
        {ShutterSpeed::MAXIMUM,
         static_cast<float>(std::pow(10, static_cast<double>(ShutterSpeed::MAXIMUM) / (6 * -10 / 3))),
         EvEntry::NON_STANDARD},
        {ShutterSpeed::MINIMUM,
         static_cast<float>(std::pow(10, static_cast<double>(ShutterSpeed::MINIMUM) / (6 * -10 / 3))),
         EvEntry::NON_STANDARD},
    };

    for (auto time : seconds_fullStop) {
        time.test(&Unit::shutterSpeedFromSeconds, &ShutterSpeed::toSeconds, "", " s");
    }

    for (auto time : seconds_partialStop) {
        time.test(&Unit::shutterSpeedFromSeconds, &ShutterSpeed::toSeconds, "", " s");
    }

    // limits
    ShutterSpeed apexShutterSpeed;

    apexShutterSpeed = ShutterSpeed(0);
    EXPECT_TRUE(Unit::shutterSpeedFromSeconds(apexShutterSpeed, 0)) << "Problem: ‘0 s’ doesn't give an overflow.";
    EXPECT_EQ(static_cast<IntegerType>(apexShutterSpeed), ShutterSpeed::MAXIMUM)
        << "Problem: ‘0 s’ has been converted to ‘" << static_cast<IntegerType>(apexShutterSpeed)
        << "’ instead of ‘ShutterSpeed::MAXIMUM’.";

    apexShutterSpeed = ShutterSpeed(0);
    EXPECT_TRUE(Unit::shutterSpeedFromSeconds(apexShutterSpeed, INFINITY))
        << "Problem: ‘∞ s’ doesn't give an overflow.";
    EXPECT_EQ(static_cast<IntegerType>(apexShutterSpeed), ShutterSpeed::MINIMUM)
        << "Problem: ‘∞ s’ has been converted to ‘" << static_cast<IntegerType>(apexShutterSpeed)
        << "’ instead of ‘ShutterSpeed::MINIMUM’.";
}

TEST(Output, seconds) {
    std::optional<double> time;
    double                expectedSeconds = NAN;

    for (IntegerType internal = ShutterSpeed::MINIMUM | 1; internal <= ShutterSpeed::MAXIMUM; internal += 2) {
        if (const IntegerType subStop = internal % 6; subStop == 3 || subStop == -3) {
            continue;
        }

        time            = ShutterSpeed(internal).toSeconds();
        expectedSeconds = std::pow(10, internal / -20.0);

        ASSERT_TRUE(time.has_value()) << "Problem: ‘ShutterSpeed(" << internal << ")’ gives no output.";
        EXPECT_EQ(*time, expectedSeconds) << std::setprecision(10) << "Problem: " << internal << " is displayed ‘"
                                          << *time << " s’ instead of ‘" << expectedSeconds << " s’.";
    }
}

TEST(RoundTrip, seconds) {
    struct {
        ShutterSpeed source, result;
    } internal;
    std::optional<double> time;

    for (IntegerType i = ShutterSpeed::MINIMUM; i <= ShutterSpeed::MAXIMUM; ++i) {
        internal.source = ShutterSpeed(i);
        internal.result = ShutterSpeed(ShutterSpeed::UNDEFINED);

        time = internal.source.toSeconds();
        ASSERT_TRUE(time.has_value()) << "Problem: ‘ShutterSpeed(" << i << ")’ gives no output.";
        ASSERT_FALSE(Unit::shutterSpeedFromSeconds(internal.result, *time))
            << "Problem: ‘" << *time << " s’ gives an overflow.";
        EXPECT_EQ(static_cast<IntegerType>(internal.source), static_cast<IntegerType>(internal.result))
            << "Problem: " << static_cast<IntegerType>(internal.source) << " → ‘" << *time << " s’ → "
            << static_cast<IntegerType>(internal.result);
    }
}

namespace Test {
TEST_F(ToString, ShutterSpeed) {
    ShutterSpeed apexShutterSpeed;
    std::wstring textShutterSpeed;

    setExtractionLocale(Unit::locale());

    setRegularExpression(L"1/([1-9]\\d*(?:'\\d+)*(?:\\.\\d*[1-9])?)");
    for (IntegerType internal = 1; internal <= ShutterSpeed::MAXIMUM; ++internal) {
        apexShutterSpeed                            = ShutterSpeed(internal);
        textShutterSpeed                            = static_cast<std::wstring>(apexShutterSpeed);
        std::tuple<std::bitset<1>, float> readValue = match<float>(textShutterSpeed, L"a shutter speed");

        if (std::get<0>(readValue)[0]) {
            ShutterSpeed recognizedShutterSpeed;
            ASSERT_FALSE(Unit::shutterSpeedFromReverseSeconds(recognizedShutterSpeed, std::get<1>(readValue)))
                << "Problem: ‘1/" << std::get<1>(readValue) << "’ gives an overflow.";
            EXPECT_EQ(apexShutterSpeed, recognizedShutterSpeed);
        }
    }

    setRegularExpression(L"([1-9]\\d*(?:'\\d+)*(?:\\.\\d*[1-9])?) s");
    for (IntegerType internal = ShutterSpeed::MINIMUM; internal <= 0; ++internal) {
        apexShutterSpeed                            = ShutterSpeed(internal);
        textShutterSpeed                            = static_cast<std::wstring>(apexShutterSpeed);
        std::tuple<std::bitset<1>, float> readValue = match<float>(textShutterSpeed, L"a time");

        if (std::get<0>(readValue)[0]) {
            ShutterSpeed recognizedShutterSpeed;
            ASSERT_FALSE(Unit::shutterSpeedFromSeconds(recognizedShutterSpeed, std::get<1>(readValue)))
                << "Problem: ‘" << std::get<1>(readValue) << " s’ gives an overflow.";
            EXPECT_EQ(apexShutterSpeed, recognizedShutterSpeed);
        }
    }
}

TEST_F(ToStream, ShutterSpeed) {
    using flag = std::ios_base;

    std::wostringstream output;
    output.imbue(Unit::locale());
    output.fill(L'·');
    output.flags(flag::hex | flag::scientific | flag::showbase | flag::showpoint | flag::showpos | flag::uppercase);

    output.setf(flag::left, flag::adjustfield);
    output << std::setw(13) << ShutterSpeed(120) << L'|';
    output << std::setw(13) << ShutterSpeed(-120) << L'|';
    output << ShutterSpeed(42) << L'|';
    output << std::setw(11) << ShutterSpeed(120) << L'|';
    output << std::setw(11) << ShutterSpeed(-120) << std::flush;
    EXPECT_EQ(output.str(), L"1/10'0000'0··|10'0000'0 s··|1/12'5|1/10'0000'0|10'0000'0 s")
        << "Left alignment, 13-character wide";
    EXPECT_EQ(output.width(), 0) << "Problem: width hasn't been reset";
    EXPECT_EQ(output.precision(), 6) << "Problem: precision has changed";
    output.str(std::wstring());

    output.setf(flag::internal, flag::adjustfield);
    output << std::setw(10) << ShutterSpeed(40) << L'|';
    output << std::setw(10) << ShutterSpeed(-40) << L'|';
    output << ShutterSpeed(-30) << L'|';
    output << std::setw(6) << ShutterSpeed(40) << L'|';
    output << std::setw(6) << ShutterSpeed(-40) << std::flush;
    EXPECT_EQ(output.str(), L"····1/10'0|····10'0 s|3'0 s|1/10'0|10'0 s") << "Justified alignment, 10-character wide";
    EXPECT_EQ(output.width(), 0) << "Problem: width hasn't been reset";
    EXPECT_EQ(output.precision(), 6) << "Problem: precision has changed";
    output.str(std::wstring());

    output.setf(flag::right, flag::adjustfield);
    output << std::setw(7) << ShutterSpeed(36) << L'|';
    output << std::setw(7) << ShutterSpeed(-36) << L'|';
    output << ShutterSpeed(48) << L'|';
    output << std::setw(5) << ShutterSpeed(36) << L'|';
    output << std::setw(5) << ShutterSpeed(-36) << std::flush;
    EXPECT_EQ(output.str(), L"··1/6'0|··6'0 s|1/25'0|1/6'0|6'0 s") << "Right alignment, 7-character wide";
    EXPECT_EQ(output.width(), 0) << "Problem: width hasn't been reset";
    EXPECT_EQ(output.precision(), 6) << "Problem: precision has changed";

    EXPECT_EQ(output.flags(), flag::right | flag::hex | flag::scientific | flag::showbase | flag::showpoint |
                                  flag::showpos | flag::uppercase)
        << "Problem: flags have changed";
}
} // namespace Test

TEST(Comparators, ShutterSpeed) {
    ShutterSpeed SIXTIETH, SECOND;

    ASSERT_FALSE(Unit::shutterSpeedFromReverseSeconds(SIXTIETH, 60)) << "Problem: ‘1/60’ gives an overflow.";
    ASSERT_FALSE(Unit::shutterSpeedFromSeconds(SECOND, 1)) << "Problem: ‘1 s’ gives an overflow.";

    ::Test::compare<ShutterSpeed>(SIXTIETH, SECOND);
}
