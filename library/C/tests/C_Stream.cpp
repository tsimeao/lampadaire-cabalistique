/*
 * Copyright ©2019 Thierry Simeao
 *
 * This file is part of Lampadaire cabalistique.
 *
 * Lampadaire cabalistique is free software: you can redistribute it and/or modify it under the terms of the GNU Affero
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * Lampadaire cabalistique is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License
 * for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with Lampadaire cabalistique.
 * If not, see <https://www.gnu.org/licenses/>.
 */
#include <cstdio>
#include <cstring>
#include <ostream>

#include "fixture/CStream.hpp"

#include "C_Stream.hpp"

namespace Test {

TEST_F(CStream, Tty) {
    char      buffer[20];
    C::Stream stream(file, true);

    // attributes
    EXPECT_TRUE(stream.isATty);
    EXPECT_TRUE(stream.isInteractive);
    EXPECT_TRUE(stream.isColorCapable);

    // reading
    std::memset(buffer, '\0', sizeof(buffer));
    ASSERT_EQ(stream.read(buffer, sizeof(buffer) - 1), 7) << "Line 1";
    EXPECT_STREQ(buffer, "Line 1\n");

    std::memset(buffer, '\0', sizeof(buffer));
    ASSERT_EQ(stream.read(buffer, sizeof(buffer) - 1), 15) << "Lines 2 & 3";
    EXPECT_STREQ(buffer, "Line 2\rLine 3\r\n");

    std::memset(buffer, '\0', sizeof(buffer));
    ASSERT_EQ(stream.read(buffer, 3), 3) << "\033[4mLin\033[0me 4";
    EXPECT_STREQ(buffer, "Lin");

    std::memset(buffer, '\0', sizeof(buffer));
    ASSERT_EQ(stream.read(buffer, sizeof(buffer) - 1), 3) << "Lin\033[4me 4\033[0m";
    EXPECT_STREQ(buffer, "e 4");

    std::memset(buffer, '\0', sizeof(buffer));
    ASSERT_EQ(stream.read(buffer, sizeof(buffer) - 1), 0) << "End of stream";
    EXPECT_STREQ(buffer, "");

    // state
    EXPECT_TRUE(static_cast<bool>(stream)) << "Open stream";
    EXPECT_FALSE(!stream) << "Open stream";

    std::fclose(file), file = nullptr;
    EXPECT_EQ(stream.read(buffer, sizeof(buffer) - 1), -1) << "Closed stream";

    EXPECT_FALSE(static_cast<bool>(stream)) << "Closed stream";
    EXPECT_TRUE(!stream) << "Closed stream";
}

TEST_F(CStream, File) {
    char      buffer[20];
    C::Stream stream(file, false);

    // attributes
    EXPECT_FALSE(stream.isATty);
    EXPECT_FALSE(stream.isInteractive);
    EXPECT_FALSE(stream.isColorCapable);

    // reading
    std::memset(buffer, '\0', sizeof(buffer));
    ASSERT_EQ(stream.read(buffer, sizeof(buffer) - 1), 19) << "“Line 1\\nLine 2\\rLine ”";
    EXPECT_STREQ(buffer, "Line 1\nLine 2\rLine ");

    std::memset(buffer, '\0', sizeof(buffer));
    ASSERT_EQ(stream.read(buffer, sizeof(buffer) - 1), 9) << "“3\\r\\nLine 4”";
    EXPECT_STREQ(buffer, "3\r\nLine 4");

    // writing
    ASSERT_EQ(std::fseek(file, 11, SEEK_SET), 0);
    stream << "arizations" << std::flush;
    std::rewind(file);

    std::memset(buffer, '\0', sizeof(buffer));
    ASSERT_EQ(stream.read(buffer, 7), 7) << "Line 1";
    EXPECT_STREQ(buffer, "Line 1\n");

    std::memset(buffer, '\0', sizeof(buffer));
    ASSERT_EQ(stream.read(buffer, 19), 19) << "Old lines 2 & 3\n\033[4mLine\033[0m 4";
    EXPECT_STREQ(buffer, "Linearizations\nLine");

    std::memset(buffer, '\0', sizeof(buffer));
    ASSERT_EQ(stream.read(buffer, sizeof(buffer) - 1), 2) << "Line\033[4m 4\033[0m";
    EXPECT_STREQ(buffer, " 4");

    // state
    EXPECT_TRUE(static_cast<bool>(stream)) << "Open stream";
    EXPECT_FALSE(!stream) << "Open stream";

    std::fclose(file), file = nullptr;
    stream << "Text." << std::endl; // error
    stream << std::flush;

    EXPECT_TRUE(static_cast<std::ostream &>(stream).fail()) << "Closed stream";
    EXPECT_FALSE(static_cast<bool>(stream)) << "Closed stream";
    EXPECT_TRUE(!stream) << "Closed stream";

    EXPECT_EQ(stream.read(buffer, sizeof(buffer) - 1), -1) << "Closed stream";
}

TEST_F(CStream, UnknownType) {
    C::Stream stream(file, nullptr);

    // attributes
    EXPECT_FALSE(stream.isATty);
    EXPECT_TRUE(stream.isInteractive);
    EXPECT_FALSE(stream.isColorCapable);
}

} // namespace Test
