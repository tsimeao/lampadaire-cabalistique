/*
 * Copyright ©2019 Thierry Simeao
 *
 * This file is part of Lampadaire cabalistique.
 *
 * Lampadaire cabalistique is free software: you can redistribute it and/or modify it under the terms of the GNU Affero
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * Lampadaire cabalistique is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License
 * for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with Lampadaire cabalistique.
 * If not, see <https://www.gnu.org/licenses/>.
 */
#pragma once
/**
 * @file
 */
#include <LampCab/Stream.hpp>
#include <lamp_cab_export.h>

#include <ostream>

namespace LampCab {

/**
 * @brief Superclass of all classes representing an output stream for the parser or the scanner
 */
class OutputStream : virtual public Stream {
public:
    const bool isColorCapable; ///< Can the stream print colors?

protected:
    /**
     * @param[in] colorizationCapable the stream can print colors
     */
    explicit OutputStream(bool colorizationCapable) noexcept : isColorCapable(colorizationCapable) {}

public:
    OutputStream(const OutputStream &) = delete;
    OutputStream &operator=(const OutputStream &) = delete;

    /**
     * @return reference to the associated standard library stream
     */
    virtual operator std::ostream &() = 0;
    /**
     * @details   Print the \a item on the associated standard library stream.
     * @param[in] item item to display
     * @return    reference to the associated standard library stream
     */
    template <typename T> std::ostream &operator<<(const T &item) { return static_cast<std::ostream &>(*this) << item; }
    /**
     * @details   Apply the \a manipulator on the associated standard library stream.
     * @param[in] manipulator \c std::ostream manipulator to apply
     * @return    reference to the associated standard library stream
     */
    std::ostream &operator<<(std::ostream &(*manipulator)(std::ostream &)) {
        return static_cast<std::ostream &>(*this) << manipulator;
    }
    /**
     * @details   Apply the \a manipulator on the associated standard library stream.
     * @param[in] manipulator \c std::ios manipulator to apply
     * @return    reference to the associated standard library stream
     */
    std::ostream &operator<<(std::ios &(*manipulator)(std::ios &)) {
        return static_cast<std::ostream &>(*this) << manipulator;
    }
    /**
     * @details   Apply the \a manipulator on the associated standard library stream.
     * @param[in] manipulator \c std::ios_base manipulator to apply
     * @return    reference to the associated standard library stream
     */
    std::ostream &operator<<(std::ios_base &(*manipulator)(std::ios_base &)) {
        return static_cast<std::ostream &>(*this) << manipulator;
    }
};

} // namespace LampCab
