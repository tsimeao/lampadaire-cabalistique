/*
 * Copyright ©2019 Thierry Simeao
 *
 * This file is part of Lampadaire cabalistique.
 *
 * Lampadaire cabalistique is free software: you can redistribute it and/or modify it under the terms of the GNU Affero
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * Lampadaire cabalistique is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License
 * for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with Lampadaire cabalistique.
 * If not, see <https://www.gnu.org/licenses/>.
 */
#pragma once
/**
 * @file
 */
#include <lamp_cab_export.h>

namespace LampCab {

/**
 * @brief Superclass of all classes representing a stream for the scanner or the parser
 */
class Stream {
public:
    const bool isATty; ///< Is the stream a TTY?

protected:
    /**
     * @param[in] tty the stream is a TTY
     */
    constexpr explicit Stream(bool tty) noexcept : isATty(tty) {}

public:
    Stream(const Stream &) = delete;
    Stream &operator=(const Stream &) = delete;
    virtual ~Stream() {}

    /**
     * @return the stream has no errors
     */
    virtual explicit operator bool() const = 0;
    /**
     * @return an error has occurred
     */
    virtual bool operator!() const = 0;
};

} // namespace LampCab
