/*
 * Copyright © 2018-2019 Thierry Simeao
 *
 * This file is part of Lampadaire cabalistique.
 *
 * Lampadaire cabalistique is free software: you can redistribute it and/or modify it under the terms of the GNU Affero
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * Lampadaire cabalistique is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License
 * for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with Lampadaire cabalistique.
 * If not, see <https://www.gnu.org/licenses/>.
 */
#pragma once
/**
 * @file
 */
#include <LampCab/Stream.hpp>
#include <lamp_cab_export.h>

#include <ios>

namespace LampCab {

/**
 * @brief Superclass of all classes representing an input stream for the scanner
 */
class InputStream : virtual public Stream {
public:
    /**
     * @details Lines are read one by one instead of waiting for an end of stream or a full buffer.
     */
    const bool isInteractive;

protected:
    /**
     * @param[in] interactive read line by line instead of waiting for an end of stream or a full buffer
     */
    explicit InputStream(bool interactive) noexcept : isInteractive(interactive) {}

public:
    InputStream(const InputStream &) = delete;
    InputStream &operator=(const InputStream &) = delete;

    /**
     * @brief      Read up to \a lengthMax bytes from the input stream into \a buffer
     * @details    It blocks until input data is available or an end of stream (or end of line if it \a isInteractive)
     *             is detected.
     * @param[out] buffer    buffer into which the data is read
     * @param[in]  lengthMax maximum number of bytes to read
     * @return     number of bytes read, possibly \c 0 if the end of the stream has been reached, or \c -1 if an error
     *             has occurred
     */
    virtual std::streamsize read(char *buffer, std::streamsize lengthMax) = 0;
};

} // namespace LampCab
