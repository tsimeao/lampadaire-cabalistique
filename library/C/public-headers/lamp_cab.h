/*
 * Copyright © 2018-2020 Thierry Simeao
 *
 * This file is part of Lampadaire cabalistique.
 *
 * Lampadaire cabalistique is free software: you can redistribute it and/or modify it under the terms of the GNU Affero
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * Lampadaire cabalistique is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License
 * for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with Lampadaire cabalistique.
 * If not, see <https://www.gnu.org/licenses/>.
 */
#pragma once
/**
 * @file
 */
#include <lamp_cab_export.h>

#include <stdio.h>

#ifdef __cplusplus
extern "C" {
#endif

// version
LAMP_CAB_EXPORT extern const char *const lamp_cab_version;  ///< Version of the library
LAMP_CAB_EXPORT extern const char *const lamp_cab_revision; ///< Revision SHA-1 digest
/**
 * @brief   Revision date
 * @details Date of the commit if the repository is clean or compilation date if dirty.
 * @note    ISO 8601 formats are used: a commit date looks like <tt>2018-07-10T15:30:08+02:00</tt> and a compilation
 *          date looks like <tt>2018-07-10T13:30:08Z</tt>.
 */
LAMP_CAB_EXPORT extern const char *const lamp_cab_revision_date;

// lexical analyzer
LAMP_CAB_EXPORT extern const char *const flex_version; ///< Version number of the lexical analyzer

// parser
LAMP_CAB_EXPORT extern const char *const bison_version; ///< Version number of the parser
/**
 * @details       Parse \a input_stream and display outputs on \a output_stream and \a error_stream.
 * @param[in,out] input_stream  stream to parse
 * @param[in,out] output_stream standard output
 * @param[in,out] error_stream  error output
 * @retval        0 success
 * @retval        1 parsing error
 * @retval        2 failure due to memory exhaustion
 */
LAMP_CAB_EXPORT int photo_parse(FILE *input_stream, FILE *output_stream, FILE *error_stream);

#ifdef __cplusplus
} // extern "C"
#endif
