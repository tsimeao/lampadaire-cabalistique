/*
 * Copyright © 2018-2020 Thierry Simeao
 *
 * This file is part of Lampadaire cabalistique.
 *
 * Lampadaire cabalistique is free software: you can redistribute it and/or modify it under the terms of the GNU Affero
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * Lampadaire cabalistique is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License
 * for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with Lampadaire cabalistique.
 * If not, see <https://www.gnu.org/licenses/>.
 */
plugins {
    `java-library`
    kotlin("jvm")
}
java {
    sourceCompatibility = JavaVersion.VERSION_1_8
    targetCompatibility = JavaVersion.VERSION_1_8
}
dependencies {
    testImplementation(kotlin("stdlib-jdk8"))
    testImplementation(kotlin("test"))
    testImplementation(kotlin("test-junit"))
    testImplementation("org.eclipse.jgit:org.eclipse.jgit:${properties["version.jGit"]}")
    testImplementation("org.slf4j:slf4j-nop:${properties["version.SLF4J"]}")
}
tasks.compileJava {
    options.headerOutputDirectory.set(buildDir.resolve("../.generated"))
}
tasks.jar {
    manifest {
        attributes("Name" to "LampJab", "Specification-Title" to "Lampadaire cabalistique - Java binding")
    }
}
tasks.javadoc {
    title = "Lampadaire cabalistique - Java™ API"
    properties["pagesDir"]?.let {
        destinationDir = file(it as String).resolve("API/Java")
    }
}

base.archivesBaseName = "LampJab"
