/*
 * Copyright © 2018-2021 Thierry Simeao
 *
 * This file is part of Lampadaire cabalistique.
 *
 * Lampadaire cabalistique is free software: you can redistribute it and/or modify it under the terms of the GNU Affero
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * Lampadaire cabalistique is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License
 * for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with Lampadaire cabalistique.
 * If not, see <https://www.gnu.org/licenses/>.
 */
package io.gitlab.tsimeao.lampadaireCabalistique

import org.eclipse.jgit.api.Git
import org.eclipse.jgit.lib.Constants
import org.eclipse.jgit.revwalk.RevCommit
import org.eclipse.jgit.storage.file.FileRepositoryBuilder
import org.junit.Assert.assertEquals
import org.junit.Assert.assertTrue
import org.junit.Test
import java.time.Instant
import java.time.ZoneId
import java.time.format.DateTimeFormatter

class VersionNumbersTest {
    @Test
    fun flexVersion_looksCorrect() = checkVersionNumber(LampCab.getFlexVersion())

    @Test
    fun bisonVersion_looksCorrect() = checkVersionNumber(LampCab.getBisonVersion())

    @Test
    fun version_looksCorrect() {
        val completeVersion = LampCab.getVersion().split(' ')

        checkVersionNumber(completeVersion.first())
        if (completeVersion.size > 1) {
            assertEquals(
                System.getProperty("os.arch").let {
                    when (it) {
                        "amd64" -> "[x86_64]"
                        "aarch64" -> "[arm64]"
                        else -> "[$it]"
                    }
                },
                completeVersion[1]
            )
        }
    }

    @Test
    fun revision_isCorrect() = checkCommit({ commit: RevCommit, isDirty: Boolean ->
        if (isDirty) {
            assertEquals("${commit.name}-dirty", LampCab.getRevision())
        } else {
            assertEquals(commit.name, LampCab.getRevision())
        }
    })

    @Test
    fun revisionDate_looksCorrect() = checkCommit({ commit: RevCommit, isDirty: Boolean ->
        val commitDate = Instant.ofEpochSecond(commit.commitTime.toLong())
        val commitDateText = commitDate.atZone(ZoneId.systemDefault()).format(DateTimeFormatter.RFC_1123_DATE_TIME)
        val revisionDate = LampCab.getRevisionDate()
        val revisionDateText = revisionDate.format(DateTimeFormatter.RFC_1123_DATE_TIME)

        if (isDirty) {
            assertTrue(
                "the revision date ($revisionDateText) should be posterior to the commit date ($commitDateText)",
                revisionDate.toInstant().compareTo(commitDate) > 0
            )
        } else {
            assertTrue(
                "the revision date is $revisionDateText and the commit date is $commitDateText",
                revisionDate.toInstant().equals(commitDate)
            )
        }
    })

    companion object {
        @JvmStatic
        private fun checkVersionNumber(versionNumber: String) {
            versionNumber.split('.').forEach {
                try {
                    it.toInt()
                } catch (exception: NumberFormatException) {
                    throw AssertionError("‘$it’ is not a number.", exception)
                }
            }
        }

        @JvmStatic
        private fun checkCommit(checker: (RevCommit, isDirty: Boolean) -> Unit) {
            val repository = FileRepositoryBuilder().readEnvironment().findGitDir().build()
            val git = Git(repository)

            try {
                checker(
                    git.log().add(repository.resolve(Constants.HEAD)).setMaxCount(1).call().first(),
                    !git.status().call().isClean
                )
            } finally {
                git.close()
                repository.close()
            }
        }
    }
}
