/*
 * Copyright © 2018, 2020 Thierry Simeao
 *
 * This file is part of Lampadaire cabalistique.
 *
 * Lampadaire cabalistique is free software: you can redistribute it and/or modify it under the terms of the GNU Affero
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * Lampadaire cabalistique is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License
 * for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with Lampadaire cabalistique.
 * If not, see <https://www.gnu.org/licenses/>.
 */
package io.gitlab.tsimeao.lampadaireCabalistique;

import java.time.OffsetDateTime;

/** Java™ API of the <em>Lampadaire cabalistique</em> library. */
public final class LampCab {
    private static native String readRevisionDate();

    /** @return version of the library */
    public static native String getVersion();

    /** @return revision SHA-1 digest */
    public static native String getRevision();

    /** @return version number of the lexical analyzer */
    public static native String getFlexVersion();

    /** @return version number of the parser */
    public static native String getBisonVersion();

    /** @return date of the commit if the repository is clean or compilation date if dirty */
    public static OffsetDateTime getRevisionDate() {
        return OffsetDateTime.parse(readRevisionDate());
    }

    static {
        System.loadLibrary("lamp-cab");
    }
}
